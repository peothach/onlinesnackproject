package common.entity;

import common.baseservice.IdEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "banner")
@AttributeOverride(name = "id", column = @Column(name = "banner_id"))
public class Banner extends IdEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    @NotNull
    private String description;

    @Column(name = "image")
    @NotNull
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

