package common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;


import common.baseservice.IdEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "product")
@AttributeOverride(name = "id", column = @Column(name = "product_id"))
public class Product extends IdEntity {

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "price")
    @NotNull
    private float price;

    @Column(name = "ingredient")
    private String ingredient;

    @NotNull
    private int priority;

    @Column(name = "percent_item")
    private int percentItem;

    @Column(name = "quantity")
    private Integer quantity;
    
    @Column(name = "status")
    private Integer status;

    @Column(name = "date_start")
    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
    private Date dateStart;

    @Column(name = "date_end")
    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
    private Date dateEnd;

    @Column(name = "created_at")
    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
    private Date createdDate;

    @Column(name = "updated_at")
    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
    private Date updatedDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    private Category categoryId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "supplier_id", referencedColumnName = "supplier_id")
    private Supplier supplierId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User userId;

    @OneToMany(mappedBy = "productId")
    private Collection<Image> image;

    public Collection<Image> getImage() {
        return image;
    }

//    public void setImage(Collection<Image> image) {
//        this.image = image;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPercentItem() {
        return percentItem;
    }

    public void setPercentItem(int percentItem) {
        this.percentItem = percentItem;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    
    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    public Supplier getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Supplier supplierId) {
        this.supplierId = supplierId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }


}