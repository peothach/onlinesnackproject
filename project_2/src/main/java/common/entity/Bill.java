package common.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import common.baseservice.IdEntity;

@Entity
@Table(name = "bill")
@AttributeOverride(name = "id", column = @Column(name = "bill_id"))
public class Bill extends IdEntity {
	@Column(name = "status")
	private int status;

	@Column(name = "create_at")
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
	private Date createdDate;

	@Column(name = "description")
	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User userId;
	
	@OneToMany(mappedBy = "billId", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<DetailedBill> detaiedBillId;
	
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}



	
	
}
