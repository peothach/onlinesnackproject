package common.entity;

import common.baseservice.IdEntity;
import lombok.Data;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "supplier")
@AttributeOverride(name = "id", column = @Column( name = "supplier_id") )
public class Supplier extends IdEntity {
	
	 @Column(name = "name")
	    private String name;
	 
	 @Column(name= "status")
	    private int status;
	 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	 
	 
}

