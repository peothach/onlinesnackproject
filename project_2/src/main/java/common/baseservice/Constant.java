package common.baseservice;

public class Constant {
    public static final String CHANGE_PASSWORD_SUCCESSFUL = "Thay đổi mật khẩu thành công";
    public static final String USER_NOT_FOUND = "Không tìm thấy User";
    public static final String BANNER_NOT_FOUND = "Không tìm thấy Banner";
    public static final String PRODUCT_NOT_FOUND = "Không tìm thấy Product";
    public static final String IMAGE_PRODUCT_NOT_FOUND = "Không tìm thấy hỉnh ảnh";
    public static final String FILE_NOT_FOUND = "Không tìm thấy file";
    public static final String FILE_SAME_FOUND = "File đã tồn tại";
    public static final String OLD_PASSWORD_NOT_MATCH = "Password cũ không đúng";
    public static final String COMPARE_PASSWORD_NOT_MATCH = "Mật khẩu mới và xác nhận mật khẩu không giống nhau";
    public static final String CREATED = "Tạo thành công";
    public static final String UPDATED = "Cập nhật thành công";
    public static final String DELETED = "Xóa thành công";
    public static final String NOT_EMPTY = "không được để trống";
}
