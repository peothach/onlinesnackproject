package common.baseservice;

import java.util.Date;

public class CheckData {
    public static boolean checkEmpty(String value){
        if(value.equals("") || value == null || value.isEmpty()){
            return true;
        }
        return false;
    }

    public static Date handleDate(Date date){
        if(date == null){
            return new Date();
        }
        return date;
    }
}