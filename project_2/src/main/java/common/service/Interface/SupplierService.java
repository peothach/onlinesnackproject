package common.service.Interface;

import java.util.List;

import common.entity.Category;
import common.entity.Supplier;

public interface SupplierService {
	public List<Supplier> getAllSupplier();
	
	public Supplier getSupplier(Long id);
   
	public Supplier addSupplier(Supplier supplier);
	
	public Supplier updateSupplier(Supplier supplier);
   
	public boolean delSupplier(long id);

	public List<Object> report (int year);
}
