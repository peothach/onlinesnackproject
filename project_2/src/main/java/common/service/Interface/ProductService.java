package common.service.Interface;

import common.dto.EmailDTO;
import common.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * format by Huy 04/03/2021
 **/
public interface ProductService {
    List<Product> getNewProduct();
    
    List<Product> getAllNewProduct();

    List<Product> getBestSellerProduct();
    
    List<Product> getAllBestSellerProduct();

    List<Product> getPercentProduct();
    
    List<Product> getAllPercentProduct();

    String sendProductEmail(EmailDTO emailDTO);

    Product findById(long id);

    List<Product> findAll();

    Product saveOrUpdate(Product product);

    void delete(long id);

    //Trung
    List<Product> FindbyPnameAndCname(String query);

    List<Product> FindbyCategoryId(Long id_category);

    List<Product> SearchProductInCategoryId(Long id_category, String name);

    List<Product> getProducyById(Long id_product);

    List<Product> getRelatedProduct(Long id_category, Long id_product);
    //cart
    Product getProduct(Long product_id);
    
}
