package common.service.Interface;

import java.util.List;

import common.entity.Bill;

import common.entity.DetailedBill;
import common.entity.User;

public interface BillService {
	
	public Bill getIdBill(Long id_bill);
	
	public boolean deleteAllProductInCart(Long id_bill);
	
	
	//cart
	public Bill getBillByUserId(Long id_user);
	
	public Bill addBill(Bill bill);
	
	public Bill getBill(Long id_bill);
	
	public Bill updateBill(Bill bill);
}
