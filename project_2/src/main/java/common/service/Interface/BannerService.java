package common.service.Interface;

import common.entity.Banner;

import java.util.List;

public interface BannerService {
    List<Banner> findAll();

    Banner saveOrUpdate(Banner banner);

    void detele(long id);

    Banner findById(long id);
}
