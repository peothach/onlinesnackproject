package common.service.Interface;

import common.entity.User;

import java.util.List;
import java.util.Optional;


/**
 * updated by Huy 04/03/2021
 **/
public interface UserService {
    // Create new User
    public User addUser(User user);

    User verifyUser(String username, String password);

    User forgetPassword(String username, String email);

    User findByUsername(String username);

    User findById(long id);

    boolean comparePassword(String password, String repeatPassword);

    void update(User user);
    
    //trung
    public User getUser(Long id);
    public List<User> findAll();
    public User updateUser(User user);
}
