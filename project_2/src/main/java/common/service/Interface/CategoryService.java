package common.service.Interface;

import java.util.List;

import common.entity.Category;


public interface CategoryService {
	public List<Category> getAllCategory();
	
	public Category getCategory(Long id);
	   
	public Category addCategory(Category category);

	public Category updateCategory(Category category);

	public boolean delCategory(long id);

	public List<List<Double>> totalRevenueByYear(int year);

	public List<Object> report(int year);
}

