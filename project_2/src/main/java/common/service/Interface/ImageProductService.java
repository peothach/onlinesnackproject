package common.service.Interface;

import common.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageProductService {
    List<Image> findAll();

    Image findById(long id);

    Image saveOrUpdate(Image image);

    void delete(long id);

    void saveImageTo(MultipartFile file);

    List<Image> findProductById(long id);

    Image findImageByProductIdAndStatus(long productId, int status);
}
