package common.service.Interface;

import java.util.List;

import common.dto.TotalAllBillDTO;
import common.entity.Bill;
import common.entity.Category;
import common.entity.DetailedBill;
import common.entity.Product;
import common.entity.Report;


public interface DetailedBillService {
	
	public List<DetailedBill> getCartByIdUser(Long id_user);
	
	public DetailedBill getProductInCart(Long id_detailed_bill);
	
	public DetailedBill updateCart(DetailedBill detailedBill);
	
	public boolean deleteProductInCart(Long id_detailed_bill);
	
	public TotalAllBillDTO viewBillDetail(Long id);
	
	public List<TotalAllBillDTO> viewBillByUser(Long id);
	
//admin
	public DetailedBill addCart(DetailedBill detailbill);
	
	public DetailedBill viewProductInCart(Long id_user, Long id_product);
	
	public List<TotalAllBillDTO> viewAllBill();

	
	public List<DetailedBill> getBill(Long id_bill);
}
