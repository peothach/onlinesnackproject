package common.service.Imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.entity.User;
import common.repository.UserRepository;
import common.service.Interface.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        // TODO Auto-generated method stub
        return userRepository.save(user);
    }

    @Override
    public User verifyUser(String username, String password) {
        User user = userRepository.verifyUser(username.trim(), password.trim());
        if (user == null) {
            throw new RuntimeException("Username hoặc password không đúng");
        }

        return user;
    }

    @Override
    public User forgetPassword(String username, String email) {
        return userRepository.verifyEmailUser(username.trim(), email.trim());
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findById(long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public boolean comparePassword(String password, String repeatPassword) {
        if (password.equalsIgnoreCase(repeatPassword)) {
            return true;
        }
        return false;
    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }
    
    //trung
    @Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		List<User> user = userRepository.findAll();
		return user;
	}
    
    @Override
	public User getUser(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id).get();
	}
    
    @Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		user = userRepository.save(user);
		return user;
	}
    
}
