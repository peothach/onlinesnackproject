package common.service.Imp;

import common.dto.EmailDTO;
import common.service.Interface.ProductService;
import common.entity.Product;
import common.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductImp implements ProductService {

    private ProductRepository productRepository;
    private JavaMailSender emailSender;

    @Autowired
    public ProductImp(ProductRepository productRepository, JavaMailSender emailSender) {
        this.productRepository = productRepository;
        this.emailSender = emailSender;
    }

    @Override
    public List<Product> getNewProduct() {
        return productRepository.getNewProduct();
    }

    @Override
    public List<Product> getBestSellerProduct() {
        return productRepository.getBestSellerProduct();
    }

    @Override
    public List<Product> getPercentProduct() {
        return productRepository.getPercentProduct();
    }

    @Override
    public List<Product> FindbyPnameAndCname(String query) {
        return productRepository.FindbyPnameAndCname(query);
    }

    @Override
    public List<Product> FindbyCategoryId(Long id_category) {
        // TODO Auto-generated method stub
        return productRepository.FindbyCategoryId(id_category);
    }

    @Override
    public List<Product> SearchProductInCategoryId(Long id_category, String name) {
        // TODO Auto-generated method stub
        return productRepository.SearchProductInCategoryId(id_category, name);
    }

    @Override
    public List<Product> getProducyById(Long id_product) {
        // TODO Auto-generated method stub
        return productRepository.getProducyById(id_product);
    }

    @Override
    public List<Product> getRelatedProduct(Long id_category, Long id_product) {
        // TODO Auto-generated method stub
        return productRepository.getRelatedProduct(id_category, id_product);
    }

    @Override
    public String sendProductEmail(EmailDTO emailDTO) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailDTO.getToYourEmail());
            message.setSubject(emailDTO.getSubject());
            message.setText(emailDTO.getSetText());
            emailSender.send(message);

            return emailDTO.getReturnText();
        }catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Product findById(long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAllWithStatus();
    }

    @Override
    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }
    

    @Override
    public void delete(long id) {
        productRepository.deleteById(id);
    }
    
  //cart
  	@Override
  	public Product getProduct(Long product_id) {
  		// TODO Auto-generated method stub
  		return  productRepository.findById(product_id).get();
  	}

	@Override
	public List<Product> getAllNewProduct() {
		// TODO Auto-generated method stub
		return productRepository.getAllNewProduct();
	}

	@Override
	public List<Product> getAllBestSellerProduct() {
		// TODO Auto-generated method stub
		return productRepository.getAllBestSellerProduct();
	}

	@Override
	public List<Product> getAllPercentProduct() {
		// TODO Auto-generated method stub
		return productRepository.getAllPercentProduct();
	}
}
