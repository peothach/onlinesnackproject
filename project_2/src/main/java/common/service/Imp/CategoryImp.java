package common.service.Imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.entity.Category;
import common.repository.CategoryRepository;
import common.service.Interface.CategoryService;


@Service
public class CategoryImp implements CategoryService{
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> getAllCategory() {
		// TODO Auto-generated method stub
		List<Category> listCategory = categoryRepository.findAll();
		return listCategory;
	}
	
	@Override
	public Category getCategory(Long id) {
		// TODO Auto-generated method stub
		
		return  categoryRepository.findById(id).orElse(null);
	}

	@Override
	public Category addCategory(Category category) {
		// TODO Auto-generated method stub
		return categoryRepository.save(category);
	}

	@Override
	public Category updateCategory(Category category) {
		// TODO Auto-generated method stub
		category = categoryRepository.save(category);
		return category;
	}

	@Override
	public boolean delCategory(long id) {
		// TODO Auto-generated method stub
		Category category = categoryRepository.getOne(id);
		if (category != null) {
			categoryRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public List<List<Double>> totalRevenueByYear(int year) {
		return categoryRepository.totalRevenueByYear(year);
	}

	@Override
	public List<Object> report(int year) {
		return categoryRepository.report(year);
	}
}

