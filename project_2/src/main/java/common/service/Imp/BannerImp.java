package common.service.Imp;

import common.entity.Banner;
import common.repository.BannerRepository;
import common.service.Interface.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerImp implements BannerService {

    private BannerRepository bannerRepository;

    @Autowired
    public BannerImp(BannerRepository bannerRepository) {
        this.bannerRepository = bannerRepository;
    }

    @Override
    public List<Banner> findAll() {
        return bannerRepository.findAll();
    }

    @Override
    public Banner saveOrUpdate(Banner banner) {
        Banner b = bannerRepository.save(banner);
        return b;
    }

    @Override
    public void detele(long id) {
        bannerRepository.deleteById(id);
    }

    @Override
    public Banner findById(long id) {
        return bannerRepository.findById(id).orElse(null);
    }
}
