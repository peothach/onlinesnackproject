package common.service.Imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.dto.TotalAllBillDTO;
import common.entity.Bill;
import common.entity.Category;
import common.entity.DetailedBill;
import common.entity.Report;

import common.repository.DetailedBillRepository;
import common.service.Interface.DetailedBillService;

@Service
public class DetailedBillImp implements DetailedBillService {

	@Autowired
	public DetailedBillRepository detailedBillRepository;

	@Override
	public List<DetailedBill> getCartByIdUser(Long id_user) {
		return detailedBillRepository.getCartByIdUser(id_user);
	};

	@Override
	public DetailedBill getProductInCart(Long id_detailed_bill) {
		// TODO Auto-generated method stub

		return detailedBillRepository.findById(id_detailed_bill).get();
	};

	@Override
	public DetailedBill updateCart(DetailedBill detailedBill) {
		detailedBill = detailedBillRepository.save(detailedBill);
		return detailedBill;
	};


	@Override
	public boolean deleteProductInCart(Long id_detailed_bill) {
		DetailedBill db = detailedBillRepository.getOne(id_detailed_bill);
		if (db != null) {
			detailedBillRepository.deleteById(id_detailed_bill);
			return true;
		}
		return false;
	}
	
	//cart
	@Override
	public DetailedBill addCart(DetailedBill detailbill) {
		// TODO Auto-generated method stub
		return detailedBillRepository.save(detailbill);
	}

	@Override
	public DetailedBill viewProductInCart(Long id_user, Long id_product) {
		// TODO Auto-generated method stub
		return detailedBillRepository.viewProductInCart(id_user, id_product);
	}

	@Override
	public List<TotalAllBillDTO> viewAllBill() {
		// TODO Auto-generated method stub
		return detailedBillRepository.viewAllBill();
	}

	@Override
	public List<DetailedBill> getBill(Long id_bill) {
		// TODO Auto-generated method stub
		return detailedBillRepository.getBill(id_bill);
	}

	@Override
	public TotalAllBillDTO viewBillDetail(Long id) {
		// TODO Auto-generated method stub
		return detailedBillRepository.viewBillDetail(id);
	}

	@Override
	public List<TotalAllBillDTO> viewBillByUser(Long id) {
		// TODO Auto-generated method stub
		return detailedBillRepository.viewBillByUser(id);
	}

}

