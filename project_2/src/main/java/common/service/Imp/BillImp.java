package common.service.Imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.entity.Bill;

import common.entity.Category;
import common.entity.DetailedBill;
import common.repository.BillRepository;
import common.service.Interface.BillService;

@Service
public class BillImp implements BillService{
	@Autowired
	public BillRepository billRepository;

	@Override
	public Bill getIdBill(Long id_bill) {
		// TODO Auto-generated method stub
		return billRepository.findById(id_bill).get();
	}

	@Override
	public boolean deleteAllProductInCart(Long id_bill) {
		// TODO Auto-generated method stub
		Bill bill = billRepository.getOne(id_bill);
		if (bill != null) {
			billRepository.deleteById(id_bill);
			return true;
		}
		return false;
	}
	
	//cart
	@Override
	public Bill getBillByUserId(Long id_user) {
		return billRepository.getBillByUserId(id_user);
	}

	@Override
	public Bill addBill(Bill bill) {
		// TODO Auto-generated method stub
		return billRepository.save(bill);
	}

	@Override
	public Bill getBill(Long id_bill) {
		// TODO Auto-generated method stub
		return billRepository.getBill(id_bill);
	}

	@Override
	public Bill updateBill(Bill bill) {
		// TODO Auto-generated method stub
		bill = billRepository.save(bill);
		return bill;
	}
	
	

}
