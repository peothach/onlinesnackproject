package common.service.Imp;

import common.entity.Image;
import common.repository.ImageProductRepository;
import common.service.Interface.ImageProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class ImageProductImp implements ImageProductService {

    private ImageProductRepository imageProductRepository;

    @Autowired
    public ImageProductImp(ImageProductRepository imageProductRepository) {
        this.imageProductRepository = imageProductRepository;
    }

    @Override
    public List<Image> findAll() {
        return imageProductRepository.findAll();
    }

    @Override
    public Image findById(long id) {
        return imageProductRepository.findById(id).orElse(null);
    }

    @Override
    public Image saveOrUpdate(Image image) {
        return imageProductRepository.save(image);
    }

    @Override
    public void delete(long id) {
        imageProductRepository.deleteById(id);
    }

    @Override
    public void saveImageTo(MultipartFile file) {
        try (InputStream inputStream = file.getInputStream()) {
            final Path path = Paths.get("D:\\3-12-2020\\back-end\\bandoanvat\\src\\main\\resources\\image");
            if(!Files.exists(path)){
                Files.createDirectories(path);
            }
            Path pathFile = path.resolve(file.getOriginalFilename());
            Files.copy(inputStream, pathFile, StandardCopyOption.REPLACE_EXISTING);
        }catch (Exception e){
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public List<Image> findProductById(long id) {
        return imageProductRepository.findProductById(id);
    }

    @Override
    public Image findImageByProductIdAndStatus(long productId, int status) {
        return imageProductRepository.findImageByProductIdAndStatus(productId, status);
    }
}

