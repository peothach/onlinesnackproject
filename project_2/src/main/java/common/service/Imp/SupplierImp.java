package common.service.Imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.entity.Category;
import common.entity.Supplier;
import common.repository.SupplierRepository;
import common.service.Interface.SupplierService;

@Service
public class SupplierImp implements SupplierService{
	@Autowired
	private SupplierRepository supplierRepository;

	@Override
	public List<Supplier> getAllSupplier() {
		// TODO Auto-generated method stub
		List<Supplier> listSupplier = supplierRepository.findAll();
		return listSupplier;
	}

	@Override
	public Supplier getSupplier(Long id) {
		// TODO Auto-generated method stub
		return supplierRepository.findById(id).get();
	}

	@Override
	public Supplier addSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		return supplierRepository.save(supplier);
	}

	@Override
	public Supplier updateSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		supplier = supplierRepository.save(supplier);
		return supplier;
	}

	@Override
	public boolean delSupplier(long id) {
		// TODO Auto-generated method stub
		Supplier supplier = supplierRepository.getOne(id);
		if (supplier != null) {
			supplierRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public List<Object> report(int year) {
		return supplierRepository.report(year);
	}
}

