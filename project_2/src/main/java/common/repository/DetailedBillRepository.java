package common.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import common.dto.TotalAllBillDTO;
import common.entity.Cart;
import common.entity.DetailedBill;
import common.entity.Report;
import common.entity.User;


@Repository
public interface DetailedBillRepository extends JpaRepository<DetailedBill, Long> {
	
	@Query(value = "select * from detailed_bill join bill on detailed_bill.bill_id = bill.bill_id where bill.user_id = :u_id and bill.status = 2", nativeQuery = true)
	List<DetailedBill> getCartByIdUser(@Param("u_id") Long id_user);
	
	@Query(value = "select * from detailed_bill join bill on detailed_bill.bill_id = bill.bill_id" +
			 " where bill.user_id = :u_id and detailed_bill.product_id = :p_id" +
			 " and bill.status = 2", nativeQuery = true)
	DetailedBill viewProductInCart(@Param("u_id") Long id_user, @Param("p_id") Long id_product);
	

	@Query(value = "select d.bill_id as billId, b.status as status, b.create_at as create_at,"+
			" sum(quanlity) as sumQuanlity, sum(quanlity * price) as sumPrice, b.user_id as user_id, u.fullname as fullname," +
			" u.address as address, u.email as email, u.phone_number as phone_number"+
			" from detailed_bill d join bill b on d.bill_id = b.bill_id " +
			" join [user] u on b.user_id = u.user_id " +
			"group by d.bill_id, b.status, b.create_at, b.user_id, u.fullname, u.address, u.email, u.phone_number", nativeQuery = true)
	List<TotalAllBillDTO> viewAllBill();
	
	@Query(value = "select d.bill_id as billId, b.status as status, b.create_at as create_at,"+
			" sum(quanlity) as sumQuanlity, sum(quanlity * (d.price*(100-p.percent_item)/100)) as sumPrice, b.user_id as user_id, u.fullname as fullname," +
			" u.address as address, u.email as email, u.phone_number as phone_number"+
			" from detailed_bill d join bill b on d.bill_id = b.bill_id " +
			" join [user] u on b.user_id = u.user_id join product p on p.product_id = d.product_id where b.bill_id = :b_id " +
			"group by d.bill_id, b.status, b.create_at, b.user_id, u.fullname, u.address, u.email, u.phone_number", nativeQuery = true)
	TotalAllBillDTO viewBillDetail(@Param("b_id") Long id);
	
	
	@Query(value = "select * from bill join detailed_bill on detailed_bill.bill_id = bill.bill_id where bill.bill_id = :b_id", nativeQuery = true)
	List<DetailedBill> getBill(@Param("b_id") Long id_bill);
	
	@Query(value = "select d.bill_id as billId, b.status as status, b.create_at as create_at, "+
			" sum(quanlity) as sumQuanlity, sum(quanlity * price) as sumPrice, b.user_id as user_id, u.fullname as fullname, "+
			" u.address as address, u.email as email, u.phone_number as phone_number"+
			" from detailed_bill d join bill b on d.bill_id = b.bill_id "+
			" join [user] u on b.user_id = u.user_id  where b.user_id =:u_id and (b.status =0 or b.status =1 or b.status =3) "+
			" group by d.bill_id, b.status, b.create_at, b.user_id, u.fullname, u.address, u.email, u.phone_number", nativeQuery = true)
	List<TotalAllBillDTO> viewBillByUser(@Param("u_id") Long id);
}
