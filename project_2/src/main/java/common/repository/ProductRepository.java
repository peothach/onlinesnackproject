package common.repository;

import common.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/** format by Huy 04/03/2021 **/
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT top(10) p.* " +
            "FROM product p WHERE p.status = 1" +
            "ORDER BY created_at DESC", nativeQuery = true)
    List<Product> getNewProduct();
    
    @Query(value = "SELECT top(50) p.* " +
            "FROM product p WHERE p.status = 1" +
            "ORDER BY created_at DESC", nativeQuery = true)
    List<Product> getAllNewProduct();

    @Query(value = "SELECT top(10) p.* " +
            "FROM detailed_bill db, product p, bill b  " +
            "WHERE db.product_id = p.product_id AND b.bill_id=db.bill_id AND b.status = 1" +
            "ORDER BY db.quanlity DESC",
            nativeQuery = true)
    List<Product> getBestSellerProduct();
    
    @Query(value = "SELECT top(50) p.* " +
            "FROM detailed_bill db, product p, bill b  " +
            "WHERE db.product_id = p.product_id AND b.bill_id=db.bill_id AND b.status = 1" +
            "ORDER BY db.quanlity DESC",
            nativeQuery = true)
    List<Product> getAllBestSellerProduct();

    @Query(value = "SELECT top(10) p.* " +
            "FROM product p WHERE p.status = 1" +
            "ORDER BY p.percent_item DESC",
            nativeQuery = true)
    List<Product> getPercentProduct();
    
    @Query(value = "SELECT p.* " +
            "FROM product p WHERE p.status = 1 and p.percent_item > 0 " +
            "ORDER BY p.percent_item DESC",
            nativeQuery = true)
    List<Product> getAllPercentProduct();

    @Query(value = "SELECT p.* " +
            "FROM product p WHERE p.status = 1" +
            "ORDER BY p.percent_item DESC",
            nativeQuery = true)
    List<Product> findAllWithStatus();

    //Trung
    @Query(value = "SELECT * FROM product join category ON product.category_id = category.category_id WHERE product.name like %" + ":p_name" + "% or category.name like %" + ":p_name" + "%", nativeQuery = true)
    List<Product> FindbyPnameAndCname(@Param("p_name") String name);
    
    @Query(value = "SELECT * FROM product join category ON product.category_id = category.category_id WHERE category.category_id = :c_id", nativeQuery = true)
    List<Product> FindbyCategoryId(@Param("c_id") Long id_category);
    
    @Query(value = "SELECT * FROM product join category ON product.category_id = category.category_id WHERE category.category_id = :c_id and product.name like %" + ":p_name" + "%", nativeQuery = true)
    List<Product> SearchProductInCategoryId(@Param("c_id") Long id_category, @Param("p_name") String name);
    
    @Query(value = "select * from product "+
    "join image on product.category_id = image.image_id "+
    "join category on product.category_id = category.category_id "+
    "where product.product_id = :p_id", nativeQuery = true)
    List<Product> getProducyById(@Param("p_id") Long id_product);
    
    @Query(value = "select * from product "+
    		"join image on product.category_id = image.image_id "+
    		"join category on product.category_id = category.category_id "+
    		"where category.category_id= :c_id and product.product_id != :p_id ", nativeQuery = true)
    	    List<Product> getRelatedProduct(@Param("c_id") Long id_category, @Param("p_id") Long id_product);
    
    
}
