package common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import common.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT u.* " +
            "FROM [user] u " +
            "WHERE upper(u.username) = upper(:username) AND upper(u.password) = upper(:password)",
            nativeQuery = true)
    User verifyUser(@Param("username") String username, @Param("password") String password);

    @Query(value = "SELECT u.* " +
            "FROM [user] u " +
            "WHERE upper(u.email) = upper(:email) AND upper(u.username) = upper(:username)",
            nativeQuery = true)
    User verifyEmailUser(@Param("username") String username, @Param("email") String email);

    @Query(value = "select * from [user] where upper(username) = upper(:username)", nativeQuery = true)
    User findByUsername(@Param("username") String username);
    
    @Query (value = "select * from [user] where status = 1", nativeQuery = true)
    List<User> findAll();
}
