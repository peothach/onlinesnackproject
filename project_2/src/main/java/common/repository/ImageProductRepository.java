package common.repository;

import common.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ImageProductRepository extends JpaRepository<Image, Long> {

    @Query(value = "SELECT i.* " +
            "FROM image i WHERE product_id = :product_id",
            nativeQuery = true)
    List<Image> findProductById(@Param("product_id") long productId);

    @Query(value = "SELECT i.* " +
            "FROM image i WHERE product_id = :product_id AND i.status = :status",
            nativeQuery = true)
    Image findImageByProductIdAndStatus(@Param("product_id") long productId, @Param("status") int status);
}
