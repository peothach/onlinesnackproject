package common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import common.entity.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "SELECT a.*" +
            "FROM f_report_category_by_year(:year) a " +
            "ORDER BY a.quantity desc ",
            nativeQuery = true)
    List<Object> report(int year);

    @Query(value = "SELECT * " +
            "FROM (select db.price * db.quanlity as 'tong', MONTH(b.create_at) as 'thang' from detailed_bill db, bill b where b.bill_id = db.bill_id and status = 1 and YEAR(b.create_at) = :year ) a " +
            "pivot( sum(tong) for thang in ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]) ) as pvtMonth",
            nativeQuery = true)
    List<List<Double>> totalRevenueByYear(int year);
}
