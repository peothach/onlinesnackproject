package common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import common.entity.Category;
import common.entity.Supplier;

import java.util.List;

@Repository
public interface SupplierRepository  extends JpaRepository<Supplier, Long>{

    @Query(value = "SELECT a.*" +
            "FROM f_report_supplier_by_year(:year) a " +
            "ORDER BY a.quantity desc ",
            nativeQuery = true)
    List<Object> report(int year);
}
