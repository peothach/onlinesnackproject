package common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import common.entity.Bill;

import common.entity.DetailedBill;


@Repository
public interface BillRepository extends JpaRepository<Bill, Long>{
	@Query(value = "select * from bill where user_id = :u_id and status =2", nativeQuery = true)
	Bill getBillByUserId(@Param("u_id") Long id_user);
	
	@Query(value = "select * from bill where bill_id = :b_id", nativeQuery = true)
	Bill getBill(@Param("b_id") Long id_bill);
}
