package common.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import common.entity.Bill;
import common.entity.Cart;
import common.entity.DetailedBill;
import common.entity.Product;
import common.entity.User;
import common.service.Interface.BillService;
import common.service.Interface.DetailedBillService;
import common.service.Interface.ProductService;
import common.service.Interface.UserService;

@RestController
@RequestMapping("api/wishlist")
@CrossOrigin
public class WishlistCotroller {
	@Autowired
	private DetailedBillService detailedBillService;

	@Autowired
	private BillService billService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProductService productService;
	
	//thêm vào giỏ hàng
		@PostMapping("/addcart")
		public ResponseEntity<?> addCart(@RequestBody Cart cart) {
			Bill databill = billService.getBillByUserId(cart.getUser_id());
			System.out.println(databill);
			if (databill == null) {
				Bill bill = new Bill();
				bill.setStatus(2);
				bill.setCreatedDate(new Date());
				bill.setDescription("Dang giao dich");
				User user = userService.getUser(cart.getUser_id());
				bill.setUserId(user);
				Bill add = billService.addBill(bill);
//				add.getId();

				DetailedBill details = new DetailedBill();
				details.setQuanlity(1);
				Product product = productService.getProduct(cart.getProduct_id());
				details.setPrice(product.getPrice());
				details.setDescription("Dang giao dich");
				details.setBillId(add);
				details.setProductId(product);
				detailedBillService.addCart(details);
				return new ResponseEntity<DetailedBill>(details, HttpStatus.OK);
				
				//

			} else {
				DetailedBill details = new DetailedBill();
				details.setQuanlity(1);
				Product product = productService.getProduct(cart.getProduct_id());
				details.setPrice(product.getPrice());
				details.setDescription("Dang giao dich");
				details.setBillId(databill);
				details.setProductId(product);
				
				DetailedBill detailsbill = detailedBillService.viewProductInCart(cart.getUser_id(), cart.getProduct_id());
				if(detailsbill != null) {
					detailsbill.setQuanlity(detailsbill.getQuanlity()+1);
					detailedBillService.updateCart(detailsbill);
					return new ResponseEntity<DetailedBill>(detailsbill, HttpStatus.OK);
				}
				detailedBillService.addCart(details);
				return new ResponseEntity<DetailedBill>(details, HttpStatus.OK);
			}
		}
}
