package common.controller;

import common.baseservice.CheckData;
import common.baseservice.Constant;
import common.dto.BannerDTO;
import common.entity.Banner;
import common.service.Interface.BannerService;
import common.service.Interface.ImageProductService;
import common.service.Interface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/banner")
@CrossOrigin
public class BannerController {

    private BannerService bannerService;
    private ImageProductService imageProductService;

    @Autowired
    public BannerController(BannerService bannerService, ImageProductService imageProductService) {
        this.bannerService = bannerService;
        this.imageProductService = imageProductService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Banner>> findAll() {
        List<Banner> listBanners = bannerService.findAll();

        return new ResponseEntity<List<Banner>>(listBanners, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Banner> findById(@PathVariable("id") long id) {
        Banner banner = bannerService.findById(id);
        if (banner == null) {
            throw new RuntimeException(Constant.BANNER_NOT_FOUND);
        }

        return new ResponseEntity<Banner>(banner, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Banner> create(@ModelAttribute BannerDTO bannerDTO) {
        Banner b = null;
        try {
            if (bannerDTO.getMultipartFile() != null && bannerDTO.getMultipartFile().getSize() != 0) {
                if(CheckData.checkEmpty(bannerDTO.getDescription()) == true){
                    throw new RuntimeException("Description " + Constant.NOT_EMPTY);
                }
                Banner banner = new Banner();
                banner.setTitle(bannerDTO.getTitle());
                banner.setDescription(bannerDTO.getDescription());
                banner.setImage(bannerDTO.getMultipartFile().getOriginalFilename());
                banner.setId(bannerDTO.getId());
                b = bannerService.saveOrUpdate(banner);
                imageProductService.saveImageTo(bannerDTO.getMultipartFile());
            } else {
                throw new RuntimeException(Constant.FILE_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Banner>(b, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Banner> update(@ModelAttribute BannerDTO bannerDTO) {
        Banner b = bannerService.findById(bannerDTO.getId());
        if (b == null) {
            throw new RuntimeException(Constant.BANNER_NOT_FOUND);
        }
        try {
            if (bannerDTO.getMultipartFile() != null && bannerDTO.getMultipartFile().getSize() != 0) {
                if(CheckData.checkEmpty(bannerDTO.getDescription()) == true){
                    throw new RuntimeException("Description " + Constant.NOT_EMPTY);
                }
                b.setTitle(bannerDTO.getTitle());
                b.setDescription(bannerDTO.getDescription());
                b.setImage(bannerDTO.getMultipartFile().getOriginalFilename());
                b.setId(bannerDTO.getId());
                bannerService.saveOrUpdate(b);
                imageProductService.saveImageTo(bannerDTO.getMultipartFile());
            } else {
                throw new RuntimeException(Constant.FILE_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Banner>(b, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            Banner banner = bannerService.findById(id);
            if (banner == null) {
                throw new RuntimeException(Constant.BANNER_NOT_FOUND);
            }

            bannerService.detele(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}

