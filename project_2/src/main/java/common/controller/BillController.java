package common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.entity.Bill;
import common.entity.Cart;
import common.entity.Category;
import common.entity.DetailedBill;
import common.entity.Product;
import common.repository.DetailedBillRepository;
import common.repository.ProductRepository;
import common.service.Interface.BillService;
import common.service.Interface.DetailedBillService;
import common.service.Interface.ProductService;

@RestController
@RequestMapping("api/bill")
@CrossOrigin
public class BillController {

	@Autowired
	private BillService billService;

	//xóa tất cả sản phẩm trong giỏ hàng
	@DeleteMapping("/deleteall/id/{id_bill}")
	public ResponseEntity<Bill> deleteAllProductInCart(@PathVariable("id_bill") Long id_bill){
		Bill dataBill = billService.getIdBill(id_bill);
		if(dataBill == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		}else {
			billService.deleteAllProductInCart(id_bill);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
	
	//cập nhập dơn hàng
	@PutMapping("/update/{id}")
    public ResponseEntity<?> updateBill(@PathVariable("id") Long id, @RequestBody Bill bill) {
        Bill dataBill = billService.getBill(id);
        if (dataBill == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            dataBill.setStatus(bill.getStatus());
            billService.updateBill(dataBill);
            return new ResponseEntity<Bill>(dataBill, HttpStatus.OK);
        }
    }
	
	 //Tìm Category theo Id
    @GetMapping("/{id}")
    public ResponseEntity<?> getBill(@PathVariable("id") long id) {
        if (billService.getBill(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            Bill bill = billService.getBill(id);
            return new ResponseEntity<Bill>(bill, HttpStatus.OK);
        }
    }
    
  //cập nhập dơn hàng
  	@PutMapping("/updateBill/{id}")
      public ResponseEntity<?> updateStatusBill(@PathVariable("id") Long id) {
          Bill dataBill = billService.getBill(id);
          if (dataBill == null) {
              return new ResponseEntity<>(HttpStatus.NOT_FOUND);
          } else {
              dataBill.setStatus(0);
              dataBill.setDescription("dang cho xu li");
              billService.updateBill(dataBill);
              return new ResponseEntity<Bill>(dataBill, HttpStatus.OK);
          }
      }
}
