package common.controller;

import common.baseservice.Constant;
import common.dto.EmailDTO;
import common.service.Interface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import common.entity.User;
import common.service.Interface.UserService;

import java.util.List;
import java.util.Optional;

/**
 * updated by Huy 04/03/2021
 **/
@RequestMapping("api/user")
@RestController
@CrossOrigin
public class UserController {

    private UserService userService;
    private ProductService productService;

    @Autowired
    public UserController(UserService userService, ProductService productService) {
        this.userService = userService;
        this.productService = productService;
    }

    // Register
    @PostMapping("/register")
    public ResponseEntity<String> addUser(@RequestBody User u) {
        User us = userService.findByUsername(u.getUsername());
        if (us != null) {
            throw new RuntimeException("Username đã tồn tại, vui lòng nhập username khác!");
        } else {
            userService.addUser(u);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestParam("username") String username,
                                      @RequestParam("password") String password) {
        User user = userService.verifyUser(username, password);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping("/forgetPassword")
    public ResponseEntity<?> forgetPassword(@RequestParam("username") String username,
                                            @RequestParam("email") String email) {
        try {
            User user = userService.forgetPassword(username, email);
            if (user == null) {
                throw new RuntimeException("Username hoặc email không tồn tại trong hệ thống, vui lòng thử lại!");
            } else {
                user.setPassword("123456");
                userService.update(user);

                EmailDTO eDTO = new EmailDTO();
                eDTO.setToYourEmail(email.trim());
                eDTO.setReturnText("Gửi email đến " + email.trim() + " thành công!");
                eDTO.setSubject("Shopping cart F.A Team");
                eDTO.setSetText("Thay đổi password thành công. Pasword của bạn đã được thay đổi thành: 123456!");
                productService.sendProductEmail(eDTO);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/changePassword")
    public ResponseEntity<?> changePassword(@RequestParam("id") long id,
                                            @RequestParam("oldPassword") String oldPassword, @RequestParam("password") String password,
                                            @RequestParam("repeatPassword") String repeatPassword) {
        User user = userService.findById(id);
        if (user == null) {
            throw new RuntimeException(Constant.USER_NOT_FOUND);
        }

        if (user.getPassword().trim().equalsIgnoreCase(oldPassword.trim())) {
            boolean comparePassword = userService.comparePassword(password.trim(), repeatPassword.trim());
            if (comparePassword == true) {
                user.setPassword(password.trim());
                userService.update(user);
            } else {
                throw new RuntimeException(Constant.COMPARE_PASSWORD_NOT_MATCH);
            }
        } else {
            throw new RuntimeException(Constant.OLD_PASSWORD_NOT_MATCH);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<User> updateUser(@Validated @RequestBody User u) {
        try {
            User user = userService.findById(u.getId());
            if (user == null) {
                throw new RuntimeException(Constant.USER_NOT_FOUND);
            }
            userService.update(u);

            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    // trung
    // Hiển thị tất cả User
    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll() {
        List<User> list = userService.findAll();
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    // Hiển thị User theo Id
    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") long id) {
        if (userService.getUser(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            User user = userService.getUser(id);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        }
    }

    // Update user
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUseer(@PathVariable("id") Long id, @RequestBody User user) {
        User dataUser = userService.getUser(id);
        if (dataUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            dataUser.setUsername(user.getUsername());
            dataUser.setPassword(user.getPassword());
            dataUser.setStatus(user.getStatus());
            dataUser.setDescription(user.getDescription());
            dataUser.setRole(user.getRole());
            dataUser.setFullname(user.getFullname());
            dataUser.setAddress(user.getAddress());
            dataUser.setEmail(user.getEmail());
            dataUser.setPhoneNumber(user.getPhoneNumber());
            userService.updateUser(dataUser);
            return new ResponseEntity<User>(dataUser, HttpStatus.OK);
        }
    }

    // dELETE user
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        User dataUser = userService.getUser(id);
        if (dataUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            dataUser.setStatus(0);
            userService.updateUser(dataUser);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}

