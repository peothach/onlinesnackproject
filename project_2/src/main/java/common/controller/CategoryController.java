package common.controller;

import java.util.ArrayList;
import java.util.List;

import common.dto.ReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import common.entity.Category;
import common.service.Interface.CategoryService;


@RestController
@RequestMapping("api/category")
@CrossOrigin
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/all")
    public ResponseEntity<List<Category>> getAll() {
        List<Category> listCategory = categoryService.getAllCategory();
        return new ResponseEntity<List<Category>>(listCategory, HttpStatus.OK);
    }

    //Tìm Category theo Id
    @GetMapping("/{id}")
    public ResponseEntity<?> getCategory(@PathVariable("id") long id) {
        if (categoryService.getCategory(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            Category cat = categoryService.getCategory(id);
            return new ResponseEntity<Category>(cat, HttpStatus.OK);
        }
    }

    //Add mới category
    @PostMapping("/create")
    public ResponseEntity<?> addCategory(@RequestBody Category category) {
        Category dataCategory = categoryService.addCategory(category);
        if (dataCategory != null) {
            return new ResponseEntity<Category>(dataCategory, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //sửa category
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable("id") Long id, @RequestBody Category category) {
        Category dataCategory = categoryService.getCategory(id);
        if (dataCategory == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            dataCategory.setName(category.getName());
            dataCategory.setIcon(category.getIcon());
            dataCategory.setStatus(category.getStatus());
            categoryService.updateCategory(dataCategory);
            return new ResponseEntity<Category>(dataCategory, HttpStatus.OK);
        }
    }

    //xóa category
    @DeleteMapping("/deletecategory/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Long id) {
        Category dataStd = categoryService.getCategory(id);
        if (dataStd == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            categoryService.delCategory(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @GetMapping("/report")
	public ResponseEntity<List<ReportDTO>> report(@RequestParam("year") int year) {
		List<Object> listObject = categoryService.report(year);
		List<ReportDTO> listCategories = new ArrayList<>();
		ReportDTO dto;
		for(int i = 0; i < listObject.size(); i++){
			Object[] row = (Object[]) listObject.get(i);
			dto = new ReportDTO();
			dto.setName(row[0].toString());
			dto.setQuantity(Integer.parseInt(row[1].toString()));
			dto.setRevenue(Double.parseDouble(row[2].toString()));
			dto.setMin(Double.parseDouble(row[3].toString()));
			dto.setMax(Double.parseDouble(row[4].toString()));
			dto.setAvg(Double.parseDouble(row[5].toString()));
			listCategories.add(dto);
		}
		return new ResponseEntity<List<ReportDTO>>(listCategories, HttpStatus.OK);
	}

	@GetMapping("/totalRevenueByYear")
	public ResponseEntity<List<List<Double>>> totalRevenueByYear(@RequestParam("year") int year) {
		List<List<Double>> listDouble = categoryService.totalRevenueByYear(year);

		return new ResponseEntity<List<List<Double>>>(listDouble, HttpStatus.OK);
	}
}

