package common.controller;


import java.util.ArrayList;
import java.util.List;

import common.dto.ReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import common.entity.Category;
import common.entity.Supplier;
import common.service.Interface.SupplierService;

@RestController
@RequestMapping("api/supplier")
@CrossOrigin
public class SupplierController {
	@Autowired
	private SupplierService supplierService;
	
	//lấy tất cả Supplier
	@GetMapping("/all")
	public ResponseEntity<List<Supplier>> getAll(){
		List<Supplier> listSupplier = supplierService.getAllSupplier();
		return new ResponseEntity<List<Supplier>>(listSupplier, HttpStatus.OK);
	}
	
	//lấy Supplier theo Id
	@GetMapping("/{id}")
	public ResponseEntity<?> getSupplier(@PathVariable("id") long id){
		if (supplierService.getSupplier(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			Supplier sup = supplierService.getSupplier(id);
			return new ResponseEntity<Supplier>(sup, HttpStatus.OK);
		}
	}
	
	//Add mới Supplier
	@PostMapping("/create")
	public ResponseEntity<?> addSupplier(@RequestBody Supplier supplier){
		Supplier dataSupplier = supplierService.addSupplier(supplier);
		if (dataSupplier != null) {
			return new ResponseEntity<Supplier>(dataSupplier, HttpStatus.OK);
		}
		return new ResponseEntity<>( HttpStatus.NOT_FOUND);
	}
	
	//Update Supplier
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateSupplier(@PathVariable("id") Long id, @RequestBody Supplier supplier){
		Supplier dataSupplier = supplierService.getSupplier(id);
		if (dataSupplier == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			dataSupplier.setName(supplier.getName());
			dataSupplier.setStatus(supplier.getStatus());
			supplierService.updateSupplier(dataSupplier);
			return new ResponseEntity<Supplier>(dataSupplier, HttpStatus.OK);
		}
	}
	
	//Xóa Supplier
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteSupplier(@PathVariable("id") Long id){
		Supplier dataStd = supplierService.getSupplier(id);
		if (dataStd == null) {
			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
		} else {
			supplierService.delSupplier(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@GetMapping("/report")
	public ResponseEntity<List<ReportDTO>> report(@RequestParam("year") int year) {
		List<Object> listObject = supplierService.report(year);
		List<ReportDTO> listCategories = new ArrayList<>();
		ReportDTO dto;
		for(int i = 0; i < listObject.size(); i++){
			Object[] row = (Object[]) listObject.get(i);
			dto = new ReportDTO();
			dto.setName(row[0].toString());
			dto.setQuantity(Integer.parseInt(row[1].toString()));
			dto.setRevenue(Double.parseDouble(row[2].toString()));
			dto.setMin(Double.parseDouble(row[3].toString()));
			dto.setMax(Double.parseDouble(row[4].toString()));
			dto.setAvg(Double.parseDouble(row[5].toString()));
			listCategories.add(dto);
		}
		return new ResponseEntity<List<ReportDTO>>(listCategories, HttpStatus.OK);
	}
}

