package common.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.baseservice.CheckData;
import common.baseservice.Constant;
import common.dto.EmailDTO;
import common.entity.Category;
import common.entity.Product;
import common.entity.Supplier;
import common.entity.User;
import common.repository.ProductRepository;
import common.service.Interface.CategoryService;
import common.service.Interface.ProductService;
import common.service.Interface.SupplierService;
import common.service.Interface.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("api/product")
@RestController
@Validated
@CrossOrigin
public class ProductController {

    ProductService productService;
    SupplierService supplierService;
    CategoryService categoryService;
    UserService userService;

    @Autowired
    public ProductController(ProductService productService, SupplierService supplierService, CategoryService categoryService, UserService userService) {
        this.productService = productService;
        this.supplierService = supplierService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @GetMapping("/dashboardProduct")
    public ResponseEntity<Map> getDashboardProduct() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("newProduct", productService.getNewProduct());
            map.put("bestSellerProduct", productService.getBestSellerProduct());
            map.put("percentProduct", productService.getPercentProduct());
            Gson gsonBuild = new GsonBuilder().setPrettyPrinting().create();

            return ResponseEntity.ok(map);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @PostMapping("/sendEmail")
    public ResponseEntity<String> getDashboardProduct(@RequestBody EmailDTO emailDTO) {
        EmailDTO eDTO = new EmailDTO();
        eDTO.setContent(emailDTO.getContent());
        eDTO.setProductId(emailDTO.getProductId());
        eDTO.setToYourEmail(emailDTO.getToYourEmail().trim());
        eDTO.setReturnText("Gửi email đến " + emailDTO.getToYourEmail() + " thành công!");
        eDTO.setSubject("Shopping cart F.A Team");
        eDTO.setSetText(emailDTO.getContent() + " " + emailDTO.getLink());

        String res = productService.sendProductEmail(eDTO);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable("id") long id){
        Product product = productService.findById(id);
        if (product == null) {
            throw new RuntimeException(Constant.PRODUCT_NOT_FOUND);
        }
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Product>> findAll() {
        List<Product> listProducts = productService.findAll();

        return new ResponseEntity<List<Product>>(listProducts, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Product> create(@Validated @RequestBody Product product) {
        Supplier sup = supplierService.getSupplier(product.getSupplierId().getId());
        if (sup == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Category category = categoryService.getCategory(product.getCategoryId().getId());
        if (category == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user = userService.findById(product.getUserId().getId());
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Product p = null;
        try {
            if(CheckData.checkEmpty(product.getName()) == true){
                throw new RuntimeException("Name " + Constant.NOT_EMPTY);
            }
            product.setCreatedDate(CheckData.handleDate(product.getCreatedDate()));
            product.setUpdatedDate(CheckData.handleDate(product.getUpdatedDate()));
            product.setDateStart(CheckData.handleDate(product.getDateStart()));
            product.setDateEnd(CheckData.handleDate(product.getDateEnd()));
            p = productService.saveOrUpdate(product);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Product>(p, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Product> update(@Validated @RequestBody Product product) {
        Product p = productService.findById(product.getId());
        if (p == null) {
            throw new RuntimeException(Constant.PRODUCT_NOT_FOUND);
        }
        Supplier sup = supplierService.getSupplier(product.getSupplierId().getId());
        if (sup == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Category category = categoryService.getCategory(product.getCategoryId().getId());
        if (category == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user = userService.findById(product.getUserId().getId());
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        try {
            if(CheckData.checkEmpty(product.getName()) == true){
                throw new RuntimeException("Name " + Constant.NOT_EMPTY);
            }
            product.setCreatedDate(CheckData.handleDate(product.getCreatedDate()));
            product.setUpdatedDate(CheckData.handleDate(product.getUpdatedDate()));
            product.setDateStart(CheckData.handleDate(product.getDateStart()));
            product.setDateEnd(CheckData.handleDate(product.getDateEnd()));
            productService.saveOrUpdate(product);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Product>(p, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            Product p = productService.findById(id);
            if (p == null) {
                throw new RuntimeException(Constant.PRODUCT_NOT_FOUND);
            }
            productService.delete(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping("/getAllNewProduct")
    public ResponseEntity<List<Product>> getAllNewProduct() {
        List<Product> listProducts = productService.getAllNewProduct();

        return new ResponseEntity<List<Product>>(listProducts, HttpStatus.OK);
    }
    
    @GetMapping("/getAllBestSellerProduct")
    public ResponseEntity<List<Product>> getAllBestSellerProduct() {
        List<Product> listProducts = productService.getAllNewProduct();

        return new ResponseEntity<List<Product>>(listProducts, HttpStatus.OK);
    }
    
    @GetMapping("/getAllPercentProduct")
    public ResponseEntity<List<Product>> getAllPercentProduct() {
        List<Product> listProducts = productService.getAllNewProduct();

        return new ResponseEntity<List<Product>>(listProducts, HttpStatus.OK);
    }

    //Trung

    //tìm kiếm sản phẩm theo tên và loại
    @GetMapping("/search/{name}")
    public ResponseEntity<?> getProduct(@PathVariable("name") String name) {

        List<Product> listProduct = productService.FindbyPnameAndCname(name);
        if (productService.FindbyPnameAndCname(name) != null) {
            return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    //hiển thị sản phẩm theo id category
    @GetMapping("/id/{idcategory}")
    public ResponseEntity<?> getProductByCategoryId(@PathVariable("idcategory") Long id_category) {

        List<Product> listProduct = productService.FindbyCategoryId(id_category);
        if (productService.FindbyCategoryId(id_category) != null) {
            return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    //tìm kiếm sản phẩm trong 1 loại
    @GetMapping("/id/{idcategory}/{nameproduct}")
    public ResponseEntity<?> getProductInCategoryId(@PathVariable("idcategory") Long id_category, @PathVariable("nameproduct") String name) {

        List<Product> listProduct = productService.SearchProductInCategoryId(id_category, name);
        if (productService.SearchProductInCategoryId(id_category, name) != null) {
            return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //hiển thị sản phẩm theo id product
    @GetMapping("/by/{idproduct}")
    public ResponseEntity<?> getProductById(@PathVariable("idproduct") Long id_product) {

        List<Product> listProduct = productService.getProducyById(id_product);
        if (productService.getProducyById(id_product) != null) {
            return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    //hiển thị sản phẩm liên quan
    @GetMapping("/by/{idcategory}/{idproduct}")
    public ResponseEntity<?> getProductById(@PathVariable("idcategory") Long id_category, @PathVariable("idproduct") Long id_product) {

        List<Product> listProduct = productService.getRelatedProduct(id_category, id_product);
        if (productService.getRelatedProduct(id_category, id_product) != null) {
            return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}

