package common.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import common.dto.TotalAllBillDTO;
import common.entity.Bill;
import common.entity.Cart;
import common.entity.Category;
import common.entity.DetailedBill;
import common.entity.Product;
import common.entity.Report;
import common.entity.User;
import common.service.Interface.BillService;
import common.service.Interface.DetailedBillService;
import common.service.Interface.ProductService;
import common.service.Interface.UserService;

@RestController
@RequestMapping("api/cart")
@CrossOrigin
public class DetailedBillController {
	@Autowired
	private DetailedBillService detailedBillService;

	@Autowired
	private BillService billService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProductService productService;
	
	//Hiển thị giỏ hàng
	@GetMapping("/{iduser}")
	public ResponseEntity<?> cart(@PathVariable("iduser") Long id_user) {
		List<DetailedBill> listCart = detailedBillService.getCartByIdUser(id_user);
		if (detailedBillService.getCartByIdUser(id_user) != null) {
			return new ResponseEntity<List<DetailedBill>>(listCart, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	};
	
	//Update quantity trng giỏ hàng, nếu quantity = 0  thì xóa khỏi giỏ hàng
	@PutMapping("/update/id/{iddetailedbill}")
	public ResponseEntity<?> updateCart(@PathVariable("iddetailedbill") Long id_detailed_bill,
			@RequestBody DetailedBill db) {
		DetailedBill dataDB = detailedBillService.getProductInCart(id_detailed_bill);
		if (dataDB != null) {
			dataDB.setQuanlity((int) db.getQuanlity());
			if (db.getQuanlity() == 0) {
				detailedBillService.deleteProductInCart(id_detailed_bill);
				return new ResponseEntity<>(HttpStatus.OK);
			}
//			Object idProduct = db.getProductId();
//			Product product = productService.getProduct((Long) idProduct);
//			if(dataDB.getQuanlity() > db.getQuanlity()) {
//				product.setQuantity(product.getQuantity() + (dataDB.getQuanlity() - db.getQuanlity()));
//				productService.saveOrUpdate(product);
//			}else {
//				product.setQuantity(product.getQuantity() - (db.getQuanlity() - dataDB.getQuanlity()));
//				productService.saveOrUpdate(product);
//			}
			detailedBillService.updateCart(dataDB);
			return new ResponseEntity<DetailedBill>(dataDB, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	};
	
	//Delete sản phẩm trong giỏ hàng
	@DeleteMapping("/delete/id/{id_detailed_bill}")
	public ResponseEntity<DetailedBill> deleteProductInCart(@PathVariable("id_detailed_bill") Long id_detailed_bill) {
		DetailedBill dataDB = detailedBillService.getProductInCart(id_detailed_bill);
		if (dataDB == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			detailedBillService.deleteProductInCart(id_detailed_bill);
			return new ResponseEntity<>(HttpStatus.OK);
		}

	}

	//thêm vào giỏ hàng
	@PostMapping("/addcart")
	public ResponseEntity<?> addCart(@RequestBody Cart cart) {
		Bill databill = billService.getBillByUserId(cart.getUser_id());
		System.out.println(databill);
		if (databill == null) {
			Bill bill = new Bill();
			bill.setStatus(2);
			bill.setCreatedDate(new Date());
			bill.setDescription("Dang giao dich");
			User user = userService.getUser(cart.getUser_id());
			bill.setUserId(user);
			Bill add = billService.addBill(bill);
//			add.getId();

			DetailedBill details = new DetailedBill();
			details.setQuanlity(1);
			Product product = productService.getProduct(cart.getProduct_id());
			details.setPrice(product.getPrice());
			details.setDescription("Dang giao dich");
			details.setBillId(add);
			details.setProductId(product);
			detailedBillService.addCart(details);
			
			product.setQuantity(product.getQuantity() - 1);
			productService.saveOrUpdate(product);
			return new ResponseEntity<DetailedBill>(details, HttpStatus.OK);
			
			//

		} else {
			DetailedBill details = new DetailedBill();
			details.setQuanlity(1);
			Product product = productService.getProduct(cart.getProduct_id());
			details.setPrice(product.getPrice());
			details.setDescription("Dang giao dich");
			details.setBillId(databill);
			details.setProductId(product);
			
			DetailedBill detailsbill = detailedBillService.viewProductInCart(cart.getUser_id(), cart.getProduct_id());
			if(detailsbill != null) {
				detailsbill.setQuanlity(detailsbill.getQuanlity()+1);
				detailedBillService.updateCart(detailsbill);
				
				product.setQuantity(product.getQuantity() - 1);
				productService.saveOrUpdate(product);
				return new ResponseEntity<DetailedBill>(detailsbill, HttpStatus.OK);
			}
			product.setQuantity(product.getQuantity() - 1);
			productService.saveOrUpdate(product);
			detailedBillService.addCart(details);
			System.out.println("bbbbbbbbbbbbbbcccccccccccccccccccccccccccbbbbbbbbbbbbbbbb");
			return new ResponseEntity<DetailedBill>(details, HttpStatus.OK);
		}
	}
	
	//xem đơn hàng
	 @GetMapping("/all")
	    public ResponseEntity<List<TotalAllBillDTO>> getAll() {
	        List<TotalAllBillDTO> listDB = detailedBillService.viewAllBill();
	        return new ResponseEntity<List<TotalAllBillDTO>>(listDB, HttpStatus.OK);
	    }
	 
	//xem tootal đơn hàng
		 @GetMapping("/all/{id_bill}")
		    public ResponseEntity<TotalAllBillDTO> getBillDetail(@PathVariable("id_bill") Long id_bill) {
			 TotalAllBillDTO listBill = detailedBillService.viewBillDetail(id_bill);
				if (listBill != null) {
					return new ResponseEntity<TotalAllBillDTO>(listBill, HttpStatus.OK);
				} else {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
		    }
	 
	//xem chi tiet bill theo id bill
	 @GetMapping("/viewBill/{id_bill}")
		public ResponseEntity<?> getBill(@PathVariable("id_bill") Long id_bill) {
			List<DetailedBill> listBill = detailedBillService.getBill(id_bill);
			if (detailedBillService.getBill(id_bill) != null) {
				return new ResponseEntity<List<DetailedBill>>(listBill, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	}; 
	
	// lay tat ca bill theo user
	 @GetMapping("/viewBillByUser/{id_user}")
	    public ResponseEntity<List<TotalAllBillDTO>> getBillByUser(@PathVariable("id_user") Long id_user) {
		 List<TotalAllBillDTO> listBill = detailedBillService.viewBillByUser(id_user);
			if (listBill != null) {
				return new ResponseEntity<List<TotalAllBillDTO>>(listBill, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	    }
}

