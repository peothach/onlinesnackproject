package common.controller;

import common.baseservice.Constant;
import common.dto.ImageProductDTO;
import common.entity.Image;
import common.entity.Product;
import common.service.Interface.ImageProductService;
import common.service.Interface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("api/imageProduct")
@CrossOrigin
public class ImageProductController {

    private ImageProductService imageProductService;
    private ProductService productService;

    @Autowired
    public ImageProductController(ImageProductService imageProductService, ProductService productService) {
        this.imageProductService = imageProductService;
        this.productService = productService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Image> findById(@PathVariable("id") long id) {
        Image image = imageProductService.findById(id);
        if (image == null) {
            throw new RuntimeException(Constant.IMAGE_PRODUCT_NOT_FOUND);
        }
        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Image>> findAll() {
        List<Image> listImages = imageProductService.findAll();

        return new ResponseEntity<List<Image>>(listImages, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Image> create(@ModelAttribute ImageProductDTO imageProductDTO) {
        Image image = null;
        try {
            Product product = productService.findById(imageProductDTO.getProductIdImage());
            if (product == null) {
                throw new RuntimeException(Constant.PRODUCT_NOT_FOUND);
            }
            if (imageProductDTO.getMultipartFile() != null && imageProductDTO.getMultipartFile().length != 0) {
                for (int i = 0; i < imageProductDTO.getMultipartFile().length; i++) {
                    image = new Image();
                    image.setId(imageProductDTO.getIdImage());
                    image.setName(imageProductDTO.getMultipartFile()[i].getOriginalFilename());
                    image.setStatus(imageProductDTO.getStatusImage());
                    image.setProductId(product);

                    imageProductService.saveOrUpdate(image);
                    imageProductService.saveImageTo(imageProductDTO.getMultipartFile()[i]);
                }
            } else {
                throw new RuntimeException(Constant.FILE_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Image> update(@ModelAttribute ImageProductDTO imageProductDTO) {
        Image image = imageProductService.findById(imageProductDTO.getIdImage());
        if (image == null) {
            throw new RuntimeException(Constant.IMAGE_PRODUCT_NOT_FOUND);
        }
        try {
            Product product = productService.findById(imageProductDTO.getProductIdImage());
            if (product == null) {
                throw new RuntimeException(Constant.PRODUCT_NOT_FOUND);
            }
            if (imageProductDTO.getMultipartFile() != null) {
                for (int i = 0; i < imageProductDTO.getMultipartFile().length; i++) {

                    image.setId(imageProductDTO.getIdImage());
                    image.setStatus(imageProductDTO.getStatusImage());
                    if(imageProductDTO.getStatusImage() == 1){
                        Image iTemp = imageProductService.findImageByProductIdAndStatus(imageProductDTO.getProductIdImage(), imageProductDTO.getStatusImage());
                        if(iTemp != null){
                            iTemp.setStatus(0);
                            imageProductService.saveOrUpdate(iTemp);
                        }
                    }
                    image.setName(imageProductDTO.getMultipartFile()[i].getOriginalFilename());

                    imageProductService.saveOrUpdate(image);
                    imageProductService.saveImageTo(imageProductDTO.getMultipartFile()[i]);
                }
            } else {
                image.setId(imageProductDTO.getIdImage());
                image.setStatus(imageProductDTO.getStatusImage());
                if(imageProductDTO.getStatusImage() == 1){
                    Image iTemp = imageProductService.findImageByProductIdAndStatus(imageProductDTO.getProductIdImage(), imageProductDTO.getStatusImage());
                    if(iTemp != null){
                        iTemp.setStatus(0);
                        imageProductService.saveOrUpdate(iTemp);
                    }
                }
                imageProductService.saveOrUpdate(image);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            Image image = imageProductService.findById(id);
            if (image == null) {
                throw new RuntimeException(Constant.IMAGE_PRODUCT_NOT_FOUND);
            }

            imageProductService.delete(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/viewImageProduct")
    public ResponseEntity<List<Image>> viewImageProduct(@RequestParam("id") long id) {
        List<Image> listImages = imageProductService.findProductById(id);
        return new ResponseEntity<List<Image>>(listImages, HttpStatus.OK);
    }
}

