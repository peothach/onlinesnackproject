package common.dto;

import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;

public  interface TotalAllBillDTO {

	int getBillId();
	
	int getStatus();
	
	String  getCreate_at();
	
	int getUser_id();
	
	String getFullname();
	
	String getAddress();
	
	String getEmail();
	
	String getPhone_number();
	
	int getSumQuanlity();
	
	int getSumPrice();
	
	
}
