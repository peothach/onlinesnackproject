package common.dto;

import org.springframework.web.multipart.MultipartFile;

public class ImageProductDTO {

	private long idImage;
    private int productIdImage;
    private int statusImage;
    MultipartFile[] multipartFile;

    public long getIdImage() {
        return idImage;
    }

    public void setIdImage(long idImage) {
        this.idImage = idImage;
    }

    public int getProductIdImage() {
        return productIdImage;
    }

    public void setProductIdImage(int productIdImage) {
        this.productIdImage = productIdImage;
    }

    public int getStatusImage() {
        return statusImage;
    }

    public void setStatusImage(int statusImage) {
        this.statusImage = statusImage;
    }

    public MultipartFile[] getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile[] multipartFile) {
        this.multipartFile = multipartFile;
    }
}
