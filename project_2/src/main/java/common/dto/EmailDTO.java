package common.dto;

public class EmailDTO {

    private int productId;
    private String content;
    private String toYourEmail;
    private String link;
    private String subject;
    private String setText;
    private String returnText;


    public EmailDTO() {
    }

    public EmailDTO(int productId, String content, String toYourEmail, String subject, String setText, String returnText) {
        this.productId = productId;
        this.content = content;
        this.toYourEmail = toYourEmail;
        this.subject = subject;
        this.setText = setText;
        this.returnText = returnText;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSetText() {
        return setText;
    }

    public void setSetText(String setText) {
        this.setText = setText;
    }

    public String getReturnText() {
        return returnText;
    }

    public void setReturnText(String returnText) {
        this.returnText = returnText;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getToYourEmail() {
        return toYourEmail;
    }

    public void setToYourEmail(String toYourEmail) {
        this.toYourEmail = toYourEmail;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

