var app = angular.module("myapp", [
  "ngRoute",
  "ngCookies",
  "angularUtils.directives.dirPagination",
]);

//------------------------------ROOT CONTROLLER--------------------------------------
//-----------------------------------------------------------------------------------

app.controller("rootController", function ($scope, $http, $rootScope) {
  //--------------------HREF-----------------------------------------
  $scope.href_url = "http://localhost:8080/";

  //--------------------------HEADER (GET ALL CATEGORIES)--------------------------

  $http.get("http://localhost:8080/api/category/all").then(function (response) {
    $scope.categories = response.data;
  });

  //---------------------------SEND REDIRECT WITH SEARCH VALUE------------------

  $scope.sendRedirectSearchValue = function () {
    //GET SEARCH VALUE

    $scope.searchName = $("#searchValue").val();

    //SEND REDIRECT
    location.href = `../search_page/view.html?name=${$scope.searchName}`;
  };

  //---------------------------CART--------------------------------------
  $rootScope.cart = [];

  //LOAD CARD IN LOCAL STORAGE

  if (JSON.parse(localStorage.getItem("cart")) != null) {
    console.log("cart in local stogr");
    $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
    // console.log($rootScope.cart.length);
  }

  //--------------------------GET CART IN LOCALSTORAGE--------------------------

  $rootScope.cart = [];

  //LOAD CARD IN LOCAL STORAGE
  if (JSON.parse(localStorage.getItem("cart")) != null) {
    $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
  }

  //---------------------------WISH LIST---------------------------------------

  $rootScope.wishList = [];

  //--------------------------------CHECK LOGIN---------------------------------

  $rootScope.checkUser = false;

  //GET USER IN LOCALSTRORAGE.
  $rootScope.user = JSON.parse(localStorage.getItem("user"));
  //CHECK USER IS LOGIN
  if ($rootScope.user != null) {
    $rootScope.checkUser = true;
    // $rootScope.quantity = JSON.parse(localStorage.getItem("quantity"));
    $http({
      method: "GET",
      url: `http://localhost:8080/api/cart/${$rootScope.user.id}`,
    })
      .then(function successCallback(response) {
        $rootScope.cartByUser = response.data;
        // alert($rootScope.cartByUser.length);
        
      //  localStorage.setItem("id_bill", $rootScope.cartByUser[0].billId.id);
      })
      .then(function () {
        
      });
  }


  //----------------------------LOG OUT-------------------------------

  //CLEAR USER IN LOCALSTORAGE
  $rootScope.logOut = function () {
    localStorage.removeItem("user");
    localStorage.removeItem("cart");
    localStorage.removeItem("id_bill");
    location.href = "../home_page/view.html";
  };

  //-----------------------------GET ID CATEGORY AND SEND REDIRECT-----------------------
  $rootScope.getCategoryID = function (categoryID) {
    $rootScope.categoryID = categoryID;
    location.href =
      "../product_by_category_page/view.html?categoryID=" + categoryID;
  };

  
});
