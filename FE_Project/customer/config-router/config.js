// CONFIG ROUTE
app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "../home_page/view.html",
      controller: "homePageController",
    })
    .when("/productByCategory", {
      templateUrl: "module/productByCategory.html",
      controller: "productbyCategoryController",
    })
    .when("/contact", {
      templateUrl: "module/contact.html",
    })
    .when("/search", {
      templateUrl: "../search_page/view.html",
      controller: "searchController",
    });
});
