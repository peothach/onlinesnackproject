app.service("$productService", function ($http) {
  let base_url = "http://localhost:8080/api/product/";

  return {
    url(path) {
      return `${base_url}/${path}`;
    },
    get(path) {
      return $http.get(this.url(path));
    },
    post(path, data) {
      return $http.post(this.url(path), data);
    },
    put(path, data) {
      return $http.put(this.url(path), data);
    },
    delete(path) {
      return $http.delete(this.url(path));
    },
  };
});
