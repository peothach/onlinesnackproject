app.controller(
  "castListController",
  function ($scope, $http, $cookies, $rootScope) {
    //---------------------SEND REDIRECT WITH PRODUCT ID--------------------------
    $scope.redirectProductID = function (productID, categoryID) {
      location.href = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${categoryID}&productID=${productID}`;
    };


    // GET ALL CATEGORY
    $scope.categories = [];

    $http({
      method: "GET",
      url: "http://localhost:8080/api/category/all",
    }).then(
      function successCallback(response) {
        $scope.categories = response.data;
      },
      function errorCallback(response) {
        response.console.error("Error load categories");
      }
    );

    // CART
    $rootScope.cartByUser = [];
    $rootScope.cart = [];
    $scope.shippingCharge = 0;
    $scope.subTotal = 0;
    $scope.totalPrice = 0;


    //WHEN USER LOGIN
    if (($scope.user = JSON.parse(localStorage.getItem("user") != null))) {
      $scope.user = JSON.parse(localStorage.getItem("user"));
      $scope.bill = JSON.parse(localStorage.getItem("id_bill"));

      //------------------------get all item in cart----------------- 
      $http({
        method: "GET",
        url: `http://localhost:8080/api/cart/${$scope.user.id}`,
      })
        .then(function successCallback(response) {
          $rootScope.cartByUser = response.data;
          //  localStorage.setItem("cartAll", JSON.stringify($scope.cartByUser));
          //  $scope.cartAll = localStorage.getItem("cartAll");
        })
        .then(function () {
          // for (i = 0; i < $rootScope.cartByUser.length; i++) {
          //   $rootScope.cart.forEach((i) => {
          //     if (i.productId != $rootScope.cartByUser[i].productId) {
          //       $rootScope.cart.push($rootScope.cartByUser[i].productId);
          //     }
          //   });
          // }
          // console.log($rootScope.cart);
        });

      //--------------------------get total bill-------------------
      $http({
        method: "GET",
        url: `http://localhost:8080/api/cart/all/${$scope.bill}`,
      })
        .then(function successCallback(response) {
          $scope.totalBill = response.data;
          if ($scope.totalBill.sumPrice > 0) {
            $scope.shippingCharge = 10000;
            $scope.subTotal = 0;
          }
        })
        .then(function () {
        });

      //-----------delete item in cart---------------- 
      $scope.deleteCart = function (BillId) {
        $http({
          method: 'DELETE',
          url: 'http://localhost:8080/api/cart/delete/id/' + BillId,
        }).then(function successCallback(response) {
          // this callback will be called asynchronously
          // when the response is available
          location.reload();
        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
      }

      //-----------delete Allitem in cart---------------- 
      $scope.deleteAllCart = function () {
        $http({
          method: 'DELETE',
          url: `http://localhost:8080/api/bill/deleteall/id/${$scope.bill}`
        }).then(function successCallback(response) {
          // this callback will be called asynchronously
          // when the response is available
          localStorage.removeItem("cart");
          localStorage.removeItem("id_bill");
          location.reload();

        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
      }

      // ----------------update quantity item-------------
      $scope.getQuantityProduct = function (id, myValue) {
        var objectItem = {
          quanlity: myValue,
        }
        $http.put('http://localhost:8080/api/cart/update/id/' + id, objectItem).then(function (response) {
          if (response.status == 200) {
            console.log("update successfull");
            init_reload();
            function init_reload() {
              setInterval(function () {
                window.location.reload();
              }, 2000);
            }
          }
        }, function (reponse) {
          console.log("update unsuccessfull");
          location.reload();
        });
      }
      //WHEN USER DOES NOT LOGIN
    } else {
      //LOAD CARD IN LOCAL STORAGE
      if (JSON.parse(localStorage.getItem("cart")) != null) {
        $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
        totalPriceMethod();
      }

      if ($rootScope.cart != null) {
        var image = null;
        for (i = 0; i < $rootScope.cart.length; i++) {
          for (j = 0; i < $rootScope.cart[i].image.length; i++) {
            if ($rootScope.cart[i].image[j].status == 1) {
              image = $rootScope.cart[i].image[j];
              $rootScope.cart[i].image = image;
              break;
            }
          }
        }

        console.log($rootScope.cart);
      }

      if ($scope.totalPrice > 0) {
        $scope.shippingCharge = 10000;
        $scope.subTotal = 0;
      }

      //CATCH EVEN INPUT
      $scope.getQuantityProduct = function (id, quantity) {
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity = quantity;
            break;
          }
        }
        $scope.totalPrice = 0;
        totalPriceMethod();
        localStorage.setItem("cart", JSON.stringify($rootScope.cart));
      };

      //REMOVE ITEM FROM CART
      $scope.removeItem = function (id) {
        for (i = 0; i < $rootScope.cart.length; i++) {
          if (($rootScope.cart[i].id = id)) {
            $rootScope.cart.splice(i, 1);
            break;
          }
        }
        if ($rootScope.cart.length == 0) {
          $scope.shippingCharge = 0;
          $scope.subTotal = 0;
        }
        $scope.totalPrice = 0;
        totalPriceMethod();
        localStorage.setItem("cart", JSON.stringify($rootScope.cart));
      };

      //REMOVE All ITEM FROM CART
      $scope.removeAllItem = function () {
        for (i = 0; i < $rootScope.cart.length; i++) {
          $rootScope.cart.splice(i, $rootScope.cart.length);
          break;
        }
        if ($rootScope.cart.length == 0) {
          $scope.shippingCharge = 0;
          $scope.subTotal = 0;
        }
        $scope.totalPrice = 0;
        totalPriceMethod();
        localStorage.removeItem("cart");
      };

      function totalPriceMethod() {
        for (i = 0; i < $rootScope.cart.length; i++) {
          $scope.totalPrice +=
            ($rootScope.cart[i].price * (100 - $rootScope.cart[i].percentItem) / 100) * $rootScope.cart[i].quantity;
        }
      }

      //REMOVE ITEM FROM CART
      $scope.removeAll = function () {
        $rootScope.cart = [];
        localStorage.removeItem("cart");
      };
    }
    //-----------------------------------------------------
    //CHECK LOGIN
    $scope.user = null;

    $scope.checkLogin = function () {
      localStorage.setItem("user", true);

      if (JSON.parse(localStorage.getItem("user")) != null) {
        $scope.user = JSON.parse(localStorage.getItem("user"));
      }
      if ($scope.user == null) {
        location.href =
          "/mironmahmud.com/vegana/1-html/ltr-version/signin-up.html";
      } else {
        location.href =
          "/mironmahmud.com/vegana/1-html/ltr-version/checkout.html";
      }
    };
  }
);
