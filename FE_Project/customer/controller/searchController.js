app.controller("searchController", function ($scope, $http) {
  //----------------------GET PRODUCT BY SEARCH VALUE--------------------------

  //SEARCH VALUE BY URL
  $scope.url_string = location.href;
  $scope.searchValue = $scope.url_string.split("name=")[1];

  if (typeof $scope.searchValue === "undefined")
    return ($scope.productBySearch = []);

  //IF SEARCH VALUE # NULL => SHOW PRODUCT RELATED
  $http
    .get("http://localhost:8080/api/product/search/" + $scope.searchValue)
    .then(function (response) {
      $scope.productBySearch = response.data;
    });

  //-----------SORT-----------------
  $scope.sort = function (keyname) {
    if (!keyname.includes(":")) {
      console.log(keyname);
      $scope.sortKey = keyname;
      $scope.reverse = true;
      return;
    }
    $scope.sortKey = keyname.split(":")[0];
    $scope.reverse = false;
  };

  //INIT SORT WHEN LOADING PAGE
  $scope.sort("id");

  //---------------------SEND REDIRECT WITH PRODUCT ID--------------------------
  $scope.redirectProductID = function (productID, categoryID) {
    location.href = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${categoryID}&productID=${productID}`;
  };

  //---------------------ADD TO CART--------------------------
  $scope.addToCart = function (id, type) {
    bootoast.toast({
      message: "Thêm vào giỏ hàng thành công",
      type: "success",
      position: "top-right",
    });

    //Get userID
  // $scope.quantity1 = localStorage.getItem("quantity");
    if (JSON.parse(localStorage.getItem("user") != null)) {
      $scope.user = JSON.parse(localStorage.getItem("user"));
      $scope.cartByUser = {
        product_id: id,
        user_id: $scope.user.id,
      };
     
      $http.post(`http://localhost:8080/api/cart/addcart`, $scope.cartByUser).then(function (response) {
        if (response.status == 200) {
          console.log("add successfull");
          $scope.data = response.data;
          localStorage.setItem("id_bill", $scope.data.billId.id)
          // var newquantity = parseInt(Number($scope.quantity1) + Number('1'));
          // JSON.parse(localStorage.setItem("quantity", newquantity));
        }
      }, function (response) {
        console.log("add unsuccessfull");
       
        // location.reload();
      });
    }

    //GET LOCALSTORAGE
    if (JSON.parse(localStorage.getItem("cart")) != null) {
      $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
      //console.log(JSON.stringify($rootScope.cart));
    }

    if (type === "searchController") {
      //CHECK EXIST YET PRODUCT_ID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.searchController.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    } 

    localStorage.setItem("cart", JSON.stringify($rootScope.cart));

    if (JSON.parse(localStorage.getItem("user")) != null) {
    }
  };

  // -----------------------------

  //---------------------SEND EMAIL---------------------------
  $scope.getProductID = function (productID, categoryID) {
    $scope.productID = productID;
    $scope.categoryID = categoryID;
  };

  $scope.shareEmail = function () {
    $scope.email = $("#emailValue").val();
    $scope.content = $("#contentValue").val();
    $scope.link = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${$scope.categoryID}&productID=${$scope.productID}`;
    var objectEmail = {
      productID: $scope.productID,
      content: $scope.content,
      toYourEmail: $scope.email,
      link: $scope.link,
    };

    console.log(objectEmail);

    $http({
      method: "POST",
      url: "http://localhost:8080/api/product/sendEmail",
      data: JSON.stringify(objectEmail),
    });

    bootoast.toast({
      message: "Gửi mail thành công!!",
      type: "success",
      position: "top-right",
    });
  };

});
