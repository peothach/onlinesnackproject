app.controller("profileController", function ($scope,$rootScope,$http) {
   
    //GET PROFILE IN LOCAL STORAGE
  if (JSON.parse(localStorage.getItem("user")) != null) {
    $rootScope.userInfo = JSON.parse(localStorage.getItem("user"));
    $rootScope.idUser = $rootScope.userInfo.id;
    console.log( $rootScope.idUser, $scope.userInfo.fullname);
  }
    
    $scope.bill = [];

//Bill manage
$scope.getAllBill = function () {
    $http({
        method: 'GET',
        url: 'http://localhost:8080/api/cart/viewBillByUser/' +  $rootScope.idUser
    }).then(function successCallback(response) {
        $scope.bill = response.data;
        // console.log(response.data.length);
        //alert(JSON.stringify($scope.product));
    }, function errorCallback(response) {
  
    });
  }
  $scope.getAllBill();
  
  //get bill by id
  
  $scope.getBillByID = function (id) {
  
    
    $http.get('http://localhost:8080/api/cart/viewBill/' + id).then(function (response) {
        if (response.status == 200) {
            $scope.viewBill = response.data; 
            $rootScope.idBill = id;
            // $scope.id = response.data.id;
            // $scope.date = response.data.creatDate;
            // $scope.fullname = response.data.billId;
            // $scope.product = response.data.productId;
            
            
        }
    }, function (response) {
        console.log("get error");
    });
  }
  
  //update status bill
  $scope.updateStatusBill = function(status){
    // var today = new Date();
    // var date = today.getDate();
    // var month = today.getMonth();
    // var year = today.getFullYear();
    // var current_date = date + "/" + month + "/" + year;
    var objectBill = {
        // create_at: "",
        // description: "",
        // user_id: "1",
        // billId: id,
        status: status
    }
    
     $http.put('http://localhost:8080/api/bill/update/' + $rootScope.idBill, objectBill).then(function (response) {
            if (response.status == 200) {
                $scope.getAllBill();;
                location.reload();
                $rootScope.toastSuccess();
                console.log("update successfull");
            }
        }, function (reponse) {
            console.log("update unsuccessfull");
        });
  }
});