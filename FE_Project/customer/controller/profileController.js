app.controller("profileController", function ($scope,$rootScope,$http) {
  //GET PROFILE IN LOCAL STORAGE
  if (JSON.parse(localStorage.getItem("user")) != null) {
    $rootScope.userInfo = JSON.parse(localStorage.getItem("user"));
    console.log($scope.userInfo.fullname);
  }
  //GET PROFILE INFORMATION
    $scope.id = $rootScope.userInfo.id,
    $scope.username = $rootScope.userInfo.username;
    $scope.password = $rootScope.userInfo.password;
    $scope.status = $rootScope.userInfo.status;
    $scope.description = $rootScope.userInfo.description;
    $scope.role = $rootScope.userInfo.role;
    $scope.fullname = $rootScope.userInfo.fullname;
    $scope.address = $rootScope.userInfo.address;
    $scope.email = $rootScope.userInfo.email;
    $scope.phoneNumber = $rootScope.userInfo.phoneNumber;

    $scope.bill = [];
 

//UPDATE PROFILE USER
$scope.updateUser = function(){

  var objectUser = {
    id : $scope.id,
    username: $scope.username,
    password: $scope.password,
    status:  $scope.status,
    description: $scope.description,
    role: $scope.role,
    fullname: $scope.fullname,
    address:  $scope.address,
    email: $rootScope.userInfo.email,
    phoneNumber: $scope.phoneNumber
}
// console.log(JSON.stringify(objectUser));
$http.put('http://localhost:8080/api/user/update/', objectUser).then(function (response) {
    if (response.status == 200) {
        localStorage.setItem("user",JSON.stringify(objectUser));
        console.log("update successfull");

        bootoast.toast({
          message: "Cập nhật thông tin thành công",
          type: "success",
          position: "top-right",
        });

    }
}, function (response) {
    console.log("update unsuccessfull");
    bootoast.toast({
      message: "Cập nhật thông tin không thành công",
      type: "error",
      position: "top-right",
    });
});

}


//CHANGE PASSWORD
$scope.changePassword = function(){
  $scope.oldPass = $scope.oldPassword;
  $scope.newPass = $scope.newPassword;
  $scope.confirmPass = $scope.confirmNewPass;
  console.log($scope.oldPass,$scope.newPass,$scope.confirmPass);

  var objectUser2 = {
    id : $scope.id,
    username: $scope.username,
    password: $scope.newPass,
    status:  $scope.status,
    description: $scope.description,
    role: $scope.role,
    fullname: $scope.fullname,
    address:  $scope.address,
    email: $rootScope.userInfo.email,
    phoneNumber: $scope.phoneNumber
}


  $http.post('http://localhost:8080/api/user/changePassword?id='+$scope.id+'&oldPassword='+$scope.oldPass+'&password='+$scope.newPass+'&repeatPassword='+$scope.confirmPass+'').then(function (response) {
            if (response.status == 200 || response.status == 202) {
              console.log(JSON.stringify(objectUser2));
              localStorage.setItem("user",JSON.stringify(objectUser2));
        bootoast.toast({
          message: "Thay đổi mật khẩu thành công",
          type: "success",
          position: "top-right",
        });
        }
        else {
          bootoast.toast({
            message: "Mật khẩu hiện tại không chính xác",
            type: "error",
            position: "top-right",
          });
        }
        }, function (response) {
          bootoast.toast({
            message: "Mật khẩu hiện tại không chính xác",
            type: "error",
            position: "top-right",
          });
         
        });
    }



  
});




app.directive("compareTo", function ()  
{  
    return {  
        require: "ngModel",  
        scope:  
        {  
            confirmPassword: "=compareTo"  
        },  
        link: function (scope, element, attributes, modelVal)  
        {  
            modelVal.$validators.compareTo = function (val)  
            {  
                return val == scope.confirmPassword;  
            };  
            scope.$watch("confirmPassword", function ()  
            {  
                modelVal.$validate();  
            });  
        }  
    };  
});  
