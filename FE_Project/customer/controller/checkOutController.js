app.controller("checkOutCtrl", function ($scope, $http, $cookies, $rootScope) {
  //------------------------------------------CHECK LOGIN-----------------------------------------

  $scope.user = null;
  $scope.shippingCharge = 0;
    $scope.subTotal = 0;
    $scope.totalPrice = 0;    

  if (JSON.parse(localStorage.getItem("user")) != null) {
    $scope.user = JSON.parse(localStorage.getItem("user"));
    $scope.bill = JSON.parse(localStorage.getItem("id_bill"));
    //------------------------get all item in cart----------------- 
      $http({
        method: "GET",
        url: `http://localhost:8080/api/cart/${$scope.user.id}`,
      })
        .then(function successCallback(response) {
          $rootScope.cartByUser = response.data;
        //  localStorage.setItem("id_bill", $rootScope.cartByUser[0].billId.id);
        })
        .then(function () {
          for (i = 0; i < $rootScope.cartByUser.length; i++) {
            $rootScope.cart.forEach((i) => {
              if (i.productId != $rootScope.cartByUser[i].productId) {
                $rootScope.cart.push($rootScope.cartByUser[i].productId);
              }
            });
          }
          console.log($rootScope.cart);
        });

     //--------------------------get total bill-------------------
    $http({
      method: "GET",
      url: `http://localhost:8080/api/cart/all/${$scope.bill}`,
    })
      .then(function successCallback(response) {
        $scope.totalBill = response.data;
        if ($scope.totalBill.sumPrice > 0) {
          $scope.shippingCharge = 10000;
          $scope.subTotal = 0;
        }
      })
      .then(function () {
      }); 

      //------------------------------PAY CART---------------------------
      $scope.payCart = function (billId) {
        $http.put('http://localhost:8080/api/bill/updateBill/' + billId).then(function (response) {
          if (response.status == 200) {
            console.log("order successfull");
            localStorage.removeItem("id_bill");
            localStorage.removeItem("cart");
            localStorage.removeItem("quantity");
            localStorage.setItem("quantity", 0);
            location.href = `http://127.0.0.1:5502/customer/template/home_page/view.html`;
            
          }
        }, function (response) {
          console.log("order unsuccessfull");
          // location.reload();
        });
      }
  }



  if ($scope.user == null) {
    location.href = "../sign-up_sign-in_page/view.html";
  }

  //-----------------------------LOAD CARD IN LOCAL STORAGE------------------------------------
  $rootScope.cart = [];
  $scope.subTotal = 0;
  $scope.totalPrice = 0;
  $scope.shippingCharge = 0;

  if (JSON.parse(localStorage.getItem("cart")) != null) {
    $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
    totalPriceMethod();
  }

  if ($scope.totalPrice > 0) {
    $scope.shippingCharge = 10.0;
    $scope.subTotal = 18.45;
  }

  //--------------------------------CATCH EVEN INPUT----------------------------------------
  $scope.getQuantityProduct = function (id, quantity) {
    for (i = 0; i < $rootScope.cart.length; i++) {
      if ($rootScope.cart[i].id == id) {
        $rootScope.cart[i].quantity = quantity;
        break;
      }
    }
    $scope.totalPrice = 0;
    totalPriceMethod();
    localStorage.setItem("cart", JSON.stringify($rootScope.cart));
  };

  //-----------------------------------REMOVE ITEM FROM CART-----------------------------------
  $scope.removeItem = function (id) {
    for (i = 0; i < $rootScope.cart.length; i++) {
      if (($rootScope.cart[i].id = id)) {
        $rootScope.cart.splice(i, 1);
        break;
      }
    }
    $scope.totalPrice = 0;
    totalPriceMethod();
    localStorage.setItem("cart", JSON.stringify($rootScope.cart));
  };

  //-----------------------------------TOTAL PRICE IN CART-----------------------------------
  function totalPriceMethod() {
    for (i = 0; i < $rootScope.cart.length; i++) {
      $scope.totalPrice +=
        $rootScope.cart[i].price * $rootScope.cart[i].quantity;
    }
  }

  //-------------------------------------REMOVE ITEM FROM CART-----------------------------------
  $scope.removeAll = function () {
    $rootScope.cart = [];
    localStorage.removeItem("cart");
  };

  $scope.order = function () {
    console.log("sss");
    console.log($scope.user);
  };
});
