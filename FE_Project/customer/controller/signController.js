app.controller("signController", function ($scope, $http, $rootScope) {
  if (localStorage.getItem("user") != null) {
    $rootScope.user = localStorage.getItem("user");
    location.href = "../home_page/view.html";
  }

  //-------------SIGN UP------------------
  $scope.getUserSignUp = function () {
    $scope.user = {
      username: $("#username").val(),
      password: $("#password").val(),
      status: 1,
      email: $("#email").val(),
      fullname: $("#fullname").val(),
      address: $("#address").val(),
      phoneNumber: $("#phoneNumber").val(),
      role: 1,
    };

    $http({
      method: "POST",
      url: "http://localhost:8080/api/user/register",
      data: JSON.stringify($scope.user),
    }).then(
      function successCallback(response) {
        $scope.userLogin = response.data;
        localStorage.setItem("user", JSON.stringify($scope.userLogin));
        location.replace("index.html");
      },
      function errorCallback(response) {
        response.console.error("Error load categories");
      }
    );
  };

  //-------------SIGN IN------------------
  $scope.message = null;

  // $scope.getQuantity = function(id){
  //   $http({
  //     method: "GET",
  //     url: `http://localhost:8080/api/cart/${id}`,
  //   })
  //     .then(function successCallback(response) {
  //       localStorage.setItem("quantity", response.data.length);
  //     })
  //     .then(function () {
        
  //     });
  // }

  $scope.getUserLogin = function () {
    $scope.user = {
      username: $("#usernameLogin").val(),
      password: $("#passwordLogin").val(),
    };
    $http({
      method: "POST",
      url:
        "http://localhost:8080/api/user/login?username=" +
        $scope.user.username +
        "&password=" +
        $scope.user.password,
    }).then(
      function successCallback(response) {
        $scope.userLogin = response.data;
        localStorage.setItem("user", JSON.stringify($scope.userLogin));
        // $scope.getQuantity(response.data.id);
        // setTimeout(() => {
          location.href = "../home_page/view.html";
        // }, 1000);
        
        
      },
      function errorCallback(response) {
        $scope.message = "Tài khoản hoặc mật khẩu không chính xác.";
        //location.href = "../sign-up_sign-in_page/view.html";
      }
    );
  };

   //-------------Forgot Password------------------
   $scope.fotgotPassword = function () {
    $http({
      method: "GET",
      url:
        "http://localhost:8080/api/user/forgetPassword?username=" + $scope.username + "&email=" + $scope.email
    }).then(
      function successCallback(response) {
        bootoast.toast({
          message: "Gửi mail thành công!!",
          type: "success",
          position: "top-right",
        });
        location.replace("/customer/template/sign-up_sign-in_page/view.html#!#signin");
      },
      function errorCallback(response) {
        $scope.message = "Tài khoản hoặc mật khẩu không chính xác.";
        alert(error.data.message);
        //location.href = "../sign-up_sign-in_page/view.html";
      }
    );
  };
});
