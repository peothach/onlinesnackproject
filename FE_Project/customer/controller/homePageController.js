app.controller("homePageController", function ($scope, $http, $rootScope) {
  //--------------------API PRODUCT -----------------------------------------
  $scope.base_url = "http://localhost:8080/api/product/";

  //--------------------HREF-----------------------------------------
  $scope.href_url = "http://localhost:8080/";

  //----------------------GET PRODUCTS IN HOMEPAGE--------------------------
  $http.get(`${$scope.base_url}dashboardProduct`).then(function (response) {
    $scope.products = response.data;
  });

  //---------------------SEND REDIRECT WITH PRODUCT ID--------------------------
  $scope.redirectProductID = function (productID, categoryID) {
    location.href = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${categoryID}&productID=${productID}`;
  };

  //---------------------ADD TO CART--------------------------
  $scope.addToCart = function (id, type) {
    bootoast.toast({
      message: "Thêm vào giỏ hàng thành công",
      type: "success",
      position: "top-right",
    });

    //Get userID
    // $scope.quantity1 = localStorage.getItem("quantity");
    if (JSON.parse(localStorage.getItem("user") != null)) {
      $scope.user = JSON.parse(localStorage.getItem("user"));
      $scope.cartByUser = {
        product_id: id,
        user_id: $scope.user.id,
      };

      $http.post(`http://localhost:8080/api/cart/addcart`, $scope.cartByUser).then(function (response) {
        if (response.status == 200) {
          console.log("add successfull");
          $scope.data = response.data;
          localStorage.setItem("id_bill", $scope.data.billId.id)
          $scope.cart1 = JSON.parse(localStorage.getItem("cart"));
        }
        localStorage.removeItem("cart");
      }, function (response) {
        console.log("add unsuccessfull");

        // location.reload();
      });
    }

    //GET LOCALSTORAGE
    if (JSON.parse(localStorage.getItem("cart")) != null) {
      $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
      //console.log(JSON.stringify($rootScope.cart));
    }

    if (type === "newProduct") {
      //CHECK EXIST YET PRODUCT_ID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.products.newProduct.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    } else if (type === "percentProduct") {
      //Check exists yet ProductID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.products.percentProduct.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    } else if (type === "bestSellerProduct") {
      //Check exists yet ProductID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.products.bestSellerProduct.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    }

    localStorage.setItem("cart", JSON.stringify($rootScope.cart));

    if (JSON.parse(localStorage.getItem("user")) != null) {
    }
  };

  // -----------------------------

  //---------------------SEND EMAIL---------------------------
  $scope.getProductID = function (productID, categoryID) {
    $scope.productID = productID;
    $scope.categoryID = categoryID;
  };

  $scope.shareEmail = function () {
    $scope.email = $("#emailValue").val();
    $scope.content = $("#contentValue").val();
    $scope.link = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${$scope.categoryID}&productID=${$scope.productID}`;
    var objectEmail = {
      productID: $scope.productID,
      content: $scope.content,
      toYourEmail: $scope.email,
      link: $scope.link,
    };

    console.log(objectEmail);

    $http({
      method: "POST",
      url: "http://localhost:8080/api/product/sendEmail",
      data: JSON.stringify(objectEmail),
    });

    bootoast.toast({
      message: "Gửi mail thành công!!",
      type: "success",
      position: "top-right",
    });
  };

  //---------------------LOAD BANNER---------------------------
  $http.get(`${$scope.href_url}api/banner/findAll`).then(function (response) {
    $scope.banner = response.data;

    // =================INSERT ITEM FROM LOCAL TO DATABASE==================
    if (JSON.parse(localStorage.getItem("user") != null) && JSON.parse(localStorage.getItem("cart") != null)) {
      $scope.user = JSON.parse(localStorage.getItem("user"));
      $scope.cart = JSON.parse(localStorage.getItem("cart"));
      $scope.object = {
        product_id: $scope.cart[0].id,
        user_id: $scope.user.id,
      }
      $http({
        method: 'POST',
        url: 'http://localhost:8080/api/cart/addcart',
        data: $scope.object
      }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        if ($scope.cart.length >= 2) {
          for (i = 1; i < $scope.cart.length; i++) {
            $scope.object = {
              product_id: $scope.cart[i].id,
              user_id: $scope.user.id,
            }
            $http.post(`http://localhost:8080/api/cart/addcart`, $scope.object);
            console.log($scope.object);
          }
        }
      }, function errorCallback(response) {

      });

      JSON.parse(localStorage.removeItem("cart"));
    }

    $http({
      method: "GET",
      url: `http://localhost:8080/api/cart/${$scope.user.id}`,
    })
      .then(function successCallback(response) {
        $rootScope.cartByUser = response.data;
        localStorage.setItem("id_bill", $rootScope.cartByUser[0].billId.id);
      })
      .then(function () {

      });
  });



  //----------------------SHOW MORE-----------------------------

});
