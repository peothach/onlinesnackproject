app.controller("ctrlProductDetailByID", function ($scope, $http, $rootScope) {
  //----------------------GET PRODUCT BY ID-----------------------------

  $scope.url_string = location.href;
  $scope.categoryID = $scope.url_string.split("categoryID=")[1].split("&")[0];
  $scope.productID = $scope.url_string.split("&productID=")[1];

  console.log($scope.categoryID);
  console.log($scope.productID);

  $http
    .get("http://localhost:8080/api/product/by/" + $scope.productID)
    .then(function (response) {
      $scope.productDetailByID = response.data;
    });
  //---------------------ADD TO CART--------------------------
  $scope.addToCart = function (id, type) {
    bootoast.toast({
      message: "Thêm vào giỏ hàng thành công",
      type: "success",
      position: "top-right",
    });

    //Get userID

    if (JSON.parse(localStorage.getItem("user") != null)) {
      $scope.user = JSON.parse(localStorage.getItem("user"));

      $scope.cartByUser = {
        product_id: id,
        user_id: $scope.user.id,
      };

      $http.post(`http://localhost:8080/api/cart/addcart`, $scope.cartByUser).then(function (response) {
        if (response.status == 200) {
          console.log("add successfull");
          $scope.data = response.data;
          localStorage.setItem("id_bill", $scope.data.billId.id)
          localStorage.removeItem("cart");
        }
      }, function (response) {
        console.log("add unsuccessfull");
        // location.reload();
      });
    }
    // if (JSON.parse(localStorage.getItem("user") != null) && JSON.parse(localStorage.getItem("cart") != null)) {
    //   $scope.user = JSON.parse(localStorage.getItem("user"));
    //   $scope.cart = JSON.parse(localStorage.getItem("cart"));
    //   for (i = 0; i < $scope.cart.length; i++) {
    //     $scope.object = {
    //       product_id: $scope.cart[i].id,
    //       user_id: $scope.user.id,
    //     }
    //     console.log($scope.object)
    //     $http.post(`http://localhost:8080/api/cart/addcart`, $scope.object);
    //     JSON.parse(localStorage.removeItem("cart"));
    //   }
    // }

    //GET LOCALSTORAGE
    if (JSON.parse(localStorage.getItem("cart")) != null) {
      $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
      //console.log(JSON.stringify($rootScope.cart));
    }

    if (type === "productDetailByID") {
      //CHECK EXIST YET PRODUCT_ID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.productDetailByID.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    }
    localStorage.setItem("cart", JSON.stringify($rootScope.cart));

    if (JSON.parse(localStorage.getItem("user")) != null) {
    }
  };
});

app.controller("ctrlRelateProduct", function ($scope, $http, $rootScope) {
  $scope.url_string = location.href;
  $scope.categoryID = $scope.url_string.split("categoryID=")[1].split("&")[0];

  $http
    .get(`http://localhost:8080/api/product/id/${$scope.categoryID}`)
    .then(function (response) {
      $scope.allProductRelate = response.data;
      console.log($scope.allProductRelate);
    });
  $scope.redirectProductID = function (productID, categoryID) {
    location.href = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${categoryID}&productID=${productID}`;
  };

  //---------------------SEND EMAIL---------------------------
  $scope.getProductID = function (productID, categoryID) {
    $scope.productID = productID;
    $scope.categoryID = categoryID;
  };

  $scope.shareEmail = function () {
    $scope.email = $("#emailValue").val();
    $scope.content = $("#contentValue").val();
    $scope.link = `http://127.0.0.1:5502/customer/template/product-detail_page/view.html?categoryID=${$scope.categoryID}&productID=${$scope.productID}`;
    var objectEmail = {
      productID: $scope.productID,
      content: $scope.content,
      toYourEmail: $scope.email,
      link: $scope.link,
    };

    console.log(objectEmail);

    $http({
      method: "POST",
      url: "http://localhost:8080/api/product/sendEmail",
      data: JSON.stringify(objectEmail),
    });

    bootoast.toast({
      message: "Gửi mail thành công!!",
      type: "success",
      position: "top-right",
    });
  };

  //---------------------ADD TO CART--------------------------
  $scope.addToCart = function (id, type) {
    bootoast.toast({
      message: "Thêm vào giỏ hàng thành công",
      type: "success",
      position: "top-right",
    });

    //Get userID

    if (JSON.parse(localStorage.getItem("user") != null)) {
      $scope.user = JSON.parse(localStorage.getItem("user"));

      $scope.cartByUser = {
        product_id: id,
        user_id: $scope.user.id,
      };

      $http.post(`http://localhost:8080/api/cart/addcart`, $scope.cartByUser).then(function (response) {
        if (response.status == 200) {
          console.log("add successfull");
          $scope.data = response.data;
          localStorage.setItem("id_bill", $scope.data.billId.id)
          localStorage.removeItem("cart");
        }
      }, function (response) {
        console.log("add unsuccessfull");
        // location.reload();
      });
    }
    
    //GET LOCALSTORAGE
    if (JSON.parse(localStorage.getItem("cart")) != null) {
      $rootScope.cart = JSON.parse(localStorage.getItem("cart"));
      //console.log(JSON.stringify($rootScope.cart));
    }

    if (type === "relatedProduct") {
      //CHECK EXIST YET PRODUCT_ID
      if ($rootScope.cart.filter((i) => i.id == id).length == 0) {
        //GET PRODUCT USER ADD
        var productAddToCart = $scope.allProductRelate.filter(
          (i) => i.id == id
        );

        //ADD QUANTITY
        productAddToCart[0].quantity = 1;

        //ADD TO CART
        $rootScope.cart.push(productAddToCart[0]);
      } else {
        //INCREASE QUANTITY
        for (i = 0; i < $rootScope.cart.length; i++) {
          if ($rootScope.cart[i].id == id) {
            $rootScope.cart[i].quantity++;
            break;
          }
        }
      }
    }
    localStorage.setItem("cart", JSON.stringify($rootScope.cart));

    if (JSON.parse(localStorage.getItem("user")) != null) {
    }
  };
});

