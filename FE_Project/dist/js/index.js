var app = angular.module("myapp", []);

app.controller("adminCtrl", function ($scope,$rootScope, $http, checkAuth){
  // $scope.test = "show dữ liệu lên đi em trai";
  // console.log($scope.test);
  $scope.check = checkAuth.getuserInfo();
  let objectUser = JSON.parse(localStorage.getItem('user'));
  $rootScope.UserID = objectUser.id;
  $rootScope.userFullname = objectUser.fullname;
  console.log( $rootScope.UserID);
  console.log( $rootScope.userFullname);

  //toast message
  $rootScope.toastSuccess = function (text){
    toastr.success('Congratulation!', text);
}
$rootScope.toastError = function(text){
    toastr.error('Something wrong!', text);
}
$rootScope.warning = function(){
    toastr.warning('Mistake');
}

$(function() {

    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = 'This is positioned in the ' + msg + '. You can also style the icon any way you like.';
    }

    var toasts = [
        new Toast('error', 'toast-bottom-full-width', 'This is positioned in the bottom full width. You can also style the icon any way you like.'),
        new Toast('info', 'toast-top-full-width', 'top full width'),
        new Toast('warning', 'toast-top-left', 'This is positioned in the top left. You can also style the icon any way you like.'),
        new Toast('success', 'toast-top-right', 'top right'),
        new Toast('warning', 'toast-bottom-right', 'bottom right'),
        new Toast('error', 'toast-bottom-left', 'bottom left')
    ];

    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 3000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;


    
})

 
});

/* Create factory to Disable Browser Back Button only after Logout */
app.factory("checkAuth", function ($location, $rootScope) {
  return {
    getuserInfo: function () {
      $rootScope.checkLogged = localStorage.getItem("isLoggedIn");
      console.log($rootScope.checkLogged);
      if (
        $rootScope.checkLogged === undefined ||
        $rootScope.checkLogged === null
      ) {
        location.replace("login.html");
      } else {

      }
    },
  };
});

// app.directive("customOnChange", function () {
//   return {
//     restrict: "A",
//     link: function (scope, element, attrs) {
//       var onChangeHandler = scope.$eval(attrs.customOnChange);
//       element.bind("change", onChangeHandler);
//     },
//   };
// });


