app.controller("ctrlSupplier", function ($scope, $http, $rootScope) {
    $scope.listSupplier = [];
    $scope.user = [];
  
    $scope.btnText = "";
    // $scope.check = checkAuth.getuserInfo();
    // console.log($scope.check);
     // $scope.user = localStorage.getItem("user");
    
    //get all List Supplier
    $scope.getAllSupplier = function () {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/api/supplier/all'
        }).then(function successCallback(response) {
            $scope.listSupplier = response.data;
            $(document).ready(function() {
                $('#dataTable').DataTable();
              });
            

        }, function errorCallback(response) {
            console.log("Error");
        });
    }
    //clear object Supplier
    $scope.Clear = function () {
        $scope.nameSupplier = "";
      
    }
    //get all List User
    $scope.getAllSupplier();


    $scope.getButton = function () {
        $scope.btnText = "Save";
        $scope.Clear();
        // alert($scope.btnText);
    }

    //insert user
    $scope.saveUpdate = function () {
        var objectSupplier = {
            name: $scope.nameSupplier,
           
        }
        console.log(objectSupplier);
        //save
        if ($scope.btnText == "Save") {
            $http.post('http://localhost:8080/api/supplier/create', objectSupplier).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllSupplier();
                    // location.reload();
                    $rootScope.toastSuccess("Created successfully");
                    console.log("insert successfull")
                }

            }, function (response) {
                console.log("error");
                $scope.toastError("Create unsuccessfully");
            });
        }
        //update
        else {
          
            $http.put('http://localhost:8080/api/supplier/update/' + $rootScope.idSupplier, objectSupplier).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllSupplier("Updated successfully");
                    // location.reload();
                    $rootScope.toastSuccess();
                    console.log("update successfull");
                }
            }, function (reponse) {
                console.log("update unsuccessfull");
                $scope.toastError("Update unsuccessfully");
            });
        }
    }


    //find by ID
    $scope.getSupplierByID = function (id) {   
        $http.get('http://localhost:8080/api/supplier/' + id).then(function (response) {
            if (response.status == 200) {
                $rootScope.idSupplier = id;
                $scope.nameSupplier = response.data.name;      
                $scope.btnText = "Update";
         
            }
        }, function (reponse) {
            console.log("get error");
        });
    }

   
//delete supplier by id
$scope.deleteSupplier = function(){
        $http.delete('http://localhost:8080/api/supplier/delete/' + $rootScope.idSupplier).then(function (response) {
            if (response.status == 200) {
                $('#dataTable').DataTable().destroy();
                console.log("sucessful")
               $scope.getAllSupplier();
            //    location.reload();
            $scope.toastSuccess("Deleted successfully");
            }
        }, function (reponse) {
            console.log("delete error");
            $scope.toastError("Delete unsuccessfully");
        });
    
    }
});