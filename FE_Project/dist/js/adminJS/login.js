app.controller("ctrlLogin",function($scope,$http,$rootScope,$location){
    //login
    $scope.login = function () {
        $rootScope.isLoggedIn = false;
        $scope.user = $("#inputUsername").val();
        $scope.pass = $("#inputPassword").val();
        $scope.userAdmin = [];
     
        $http.post('http://localhost:8080/api/user/login?username='+$scope.user+'&password='+$scope.pass).then(function (response) {
            if (response.status == 200 || response.status == 202) {
                $scope.userAdmin = response.data;
                console.log(JSON.stringify($scope.userAdmin));
                if(response.data.role == 1){       
                    localStorage.setItem('user', JSON.stringify($scope.userAdmin)); 
                    $rootScope.isLoggedIn = true;  
                    localStorage.setItem("isLoggedIn", $rootScope.isLoggedIn);
                    location.replace("index.html");
                
                }
               else {
                $rootScope.isLoggedIn = false;
                   console.log("Permission Denied");
               } 
        }
        else {
            $rootScope.isLoggedIn = false;
            localStorage.setItem("isLoggedIn", $rootScope.isLoggedIn);
        }
        }, function (response) {
            console.log("error");
         
        });
    }
    //logout
    $scope.logout = function(){
        // window.localStorage.removeItem('user');
        $rootScope.isLoggedIn = false;
        localStorage.clear();
        location.replace("login.html");
    }
 
});