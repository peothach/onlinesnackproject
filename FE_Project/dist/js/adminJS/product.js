app.controller("ctrlProduct", function ($scope, $rootScope, $http, $filter) {
  $scope.product = [];
  $scope.category = [];
  $scope.supplier = [];

  $scope.getCount = function(strCat){
    return filterFilter( $scope.product, {comic:strCat}).length;
  }
  //$scope.product1 = [];
  //get all product
  $scope.getAllProduct = function () {
    $http({
      method: "GET",
      url: "http://localhost:8080/api/product/findAll",
    }).then(
      function successCallback(response) {
      
        $scope.product = response.data;
        // console.log($scope.product)
        $(document).ready(function() {
          $('#dataTable').DataTable();
        });
      
    
      },
      function errorCallback(response) {}
    );
  };
  $scope.getAllProduct();

  //get all category
  $scope.getAllCategory = function () {
    $http({
      method: "GET",
      url: "http://localhost:8080/api/category/all",
    }).then(
      function successCallback(response) {
        $scope.category = response.data;
        //  alert(JSON.stringify($scope.category));
      },
      function errorCallback(response) {}
    );
  };
  $scope.getAllCategory();
  //get all supplier
  $scope.getAllSupplier = function () {
    $http({
      method: "GET",
      url: "http://localhost:8080/api/supplier/all",
    }).then(
      function successCallback(response) {
        $scope.supplier = response.data;
        //alert(JSON.stringify($scope.supplier));
      },
      function errorCallback(response) {}
    );
  };
  $scope.getAllSupplier();
  //clear
  $scope.Clear = function () {
    $scope.name = "";
    $scope.price = "";
    $scope.ingredient = "";
    $scope.priority = "";
    $scope.percentItem = "";
    $scope.quantity = "";
    $scope.dateStart = "";
    $scope.dateEnd = "";
  };

  $scope.getButton = function () {
    $scope.btnText = "Save";
    $scope.Clear();
    // alert($scope.btnText);
  };
  //update image
  $scope.uploadFile = function (event) {
    var files = event.target.files;
    $scope.image = files[0];
    console.log($scope.image);
  };

  //
  $scope.saveUpdate = function () {
    var today = new Date();
    var date = today.getDate();
    var month = today.getMonth();
    var year = today.getFullYear();
    var current_date = date + "/" + month + "/" + year;

    var config = {
      headers: { "Content-Type": undefined },
    };
    // var objectDateEnd = new Date();
    // var objectDateStart = new Date();
    // objectDateEnd = $filter("date")($scope.dateEnd, "dd/MM/yyyy");
    // objectDateStart = $filter("date")($scope.dateStart, "dd/MM/yyyy");
    // $scope.dateEnd = objectDateEnd;
    // $scope.dateStart = objectDateStart;
    $scope.dateEnd = $filter("date")($scope.dateEnd, "dd/MM/yyyy");
    $scope.dateStart = $filter("date")($scope.dateStart, "dd/MM/yyyy");
   

    // console.log($scope.dateStart);
    //    console.log($scope.dateEnd);

    // var Start = new Date($scope.dateStart);
    // var dateStart = Start.getDate();
    // var monthStart = Start.getMonth();
    // var yearStart = Start.getFullYear();

    // var dateStart = dateStart + "/" + monthStart + "/" + yearStart;
    // $scope.dateStart = dateStart;


    // var End = new Date($scope.dateStart);
    // var dateEnd = End.getDate();
    // var monthEnd = End.getMonth();
    // var yearEnd = End.getFullYear();

    // var dateEnd = dateEnd + "/" + monthEnd + "/" + yearEnd;

    // $scope.dateEnd = dateEnd;

    var objectProduct = {
      name: $scope.name,
      price: $scope.price,
      ingredient: $scope.ingredient,
      priority: $scope.priority,
      percentItem: $scope.percentItem,
      quantity: $scope.quantity,
      dateStart: $scope.dateStart,
      dateEnd: $scope.dateEnd,
      createdDate: current_date,
      updatedDate: current_date,
      status: 1,
      categoryId: { id: $scope.categoryId },
      supplierId: { id: $scope.supplierId },
      userId: { id: 1 },
    };

    var objectProduct2 = {
      id: $rootScope.idProduct,
      name: $scope.name,
      price: $scope.price,
      ingredient: $scope.ingredient,
      priority: $scope.priority,
      percentItem: $scope.percentItem,
      quantity: $scope.quantity,
      dateStart: $scope.dateStart,
      dateEnd: $scope.dateEnd,
      createdDate: current_date,
      updatedDate: current_date,
      status: 1,
      categoryId: { id: $scope.categoryId },
      supplierId: { id: $scope.supplierId },
      userId: { id: $rootScope.UserID },
    };

    //save
    if ($scope.btnText == "Save") {
      $http
        .post("http://localhost:8080/api/product/create", objectProduct)
        .then(
          function (response) {
            console.log(JSON.stringify(objectProduct));
            if (response.status == 200) {
              $scope.product_Id = response.data.id;
              // console.log($scope.product_Id);
              // console.log(JSON.stringify(objectProduct));
              
              $('#dataTable').DataTable().destroy();
              $scope.getAllProduct();
              $rootScope.toastSuccess("Created successfully");
              $scope.Clear();
              console.log("insert successfull");
            }
          },
          function (response) {
            console.log("error");
            $scope.toastError("Create unsucessfully");
          }
        );
    } else {
      //update product
      $http
        .put("http://localhost:8080/api/product/update/", objectProduct2, {
          headers: {
              'Content-Type': 'application/json',
          }})
        .then(
          function (response) {
            if (response.status == 200) {
              $('#dataTable').DataTable().destroy();            
              $scope.getAllProduct();
              $rootScope.toastSuccess("Updated successfully");
              console.log("update successfull");
            }
          },
          function (response) {
            // console.log(response.message)
            $rootScope.toastError("Update unsuccessfully");
            console.log("update unsuccessfull");
          }
        );
    }
  };
  //get ID view img
  $scope.getIDToViewImg = function (id) {
    $rootScope.viewID = id;
    $http.get("http://localhost:8080/api/product/" + $rootScope.viewID).then(
      function (response) {
        if (response.status == 200) {
          $rootScope.nameProduct = response.data.name;
          location.replace("images.html");
          localStorage.setItem("viewID", $rootScope.viewID);
          localStorage.setItem("nameProduct", $rootScope.nameProduct);
        }
      },
      function (reponse) {
        console.log("get error");
        $scope.toastError("Error");
      }
    );
  };
  //get id to delete
  $scope.getIDToDelete = function (id) {
    $rootScope.idP = id;
  };
  //delete product
  $scope.deleteProduct = function () {
    $http
      .delete("http://localhost:8080/api/product/delete/" + $rootScope.idP)
      .then(
        function (response) {
          if (response.status == 200) {
            console.log("sucessful");
            $('#dataTable').DataTable().destroy();
            $scope.getAllProduct();
            $rootScope.toastSuccess("Deleted successfully");
            //    location.reload();
          }
        },
        function (reponse) {
          console.log("delete error");
          $scope.toastError("Delete unsuccessfully");
        }
      );
  };
  //get product by id
  $scope.getProductByID = function (id) {
    $rootScope.idProduct = id;
 

    $http.get("http://localhost:8080/api/product/" + $scope.idProduct).then(
      function (response) {
        if (response.status == 200) {
          var dateS = response.data.dateStart;
          var dateParts = dateS.split("/");

          // month is 0-based, that's why we need dataParts[1] - 1
          var dateObject = new Date(
            +dateParts[2],
            dateParts[1] - 1,
            +dateParts[0]
          );

          var dateE = response.data.dateEnd;
          var dateParts2 = dateE.split("/");

          // month is 0-based, that's why we need dataParts[1] - 1
          var dateObject2 = new Date(
            +dateParts2[2],
            dateParts2[1] - 1,
            +dateParts2[0]
          );

          $scope.name = response.data.name;
          $scope.price = response.data.price;
          $scope.ingredient = response.data.ingredient;
          $scope.priority = response.data.priority;
          $scope.percentItem = response.data.percentItem;
          $scope.quantity = response.data.quantity;
          // console.log(response.data.dateStart);
          // $scope.dateStart = new Date(response.data.dateStart);
          // console.log($scope.dateStart);
          $scope.dateStart = dateObject;
          $scope.dateEnd = dateObject2;
          // console.log($scope.dateStart);
          // $scope.dateEnd = new Date(response.data.dateEnd).toString;
          // console.log($scope.dataEnd);
          // $scope.dateEnd = new Date(response.data.dateEnd);
          // console.log($scope.dateEnd);

          // var cate = JSON.parse(response.data.categoryId.id);
          //alert(cate);
          // console.log(JSON.stringify(response.data.categoryId));
          $scope.categoryId = response.data.categoryId.id;
          $scope.nameCategory = response.data.categoryId.name;
          $scope.supplierId = response.data.supplierId.id;
          $scope.nameSupplier = response.data.supplierId.name;
          $scope.btnText = "Update";
        }
      },
      function (reponse) {
        console.log("get error");
      }
    );
  };

});
