app.controller("ctrlUser", function ($scope,$rootScope, $http) {
    $scope.listUser = [];
    $scope.user = [];
    $scope.btnText = "";
    $rootScope.User = [];

    //get all List User
    $scope.getAllUser = function () {

        $http({
            method: 'GET',
            url: 'http://localhost:8080/api/user/all'
        }).then(function successCallback(response) {
            $scope.listUser = response.data;
            $(document).ready(function() {
                $('#dataTable').DataTable();
              });



        }, function errorCallback(response) {
            console.log("Error");
        });
    }
    //clear object user
    $scope.Clear = function () {
        $scope.username = "";
        $scope.password = "";
        $scope.description = "";
        $scope.role = "";
        $scope.fullname = "";
        $scope.address = "";
        $scope.email = "";
        $scope.phone = "";
    }
    //get all List User
    $scope.getAllUser();


    $scope.getButton = function () {
        $scope.btnText = "Save";
        console.log($scope.btnText);
        $scope.Clear();
        // alert($scope.btnText);
    }

    //insert user
    $scope.saveUpdate = function () {
        var objectUser = {
            username: $scope.username,
            password: $scope.password,
            status: "1",
            description: $scope.description,
            role: $scope.role,
            fullname: $scope.fullname,
            address: $scope.address,
            email: $scope.email,
            phoneNumber: $scope.phone
        }
        //save
        if ($scope.btnText == "Save") {
            console.log(JSON.stringify(objectUser));
            $http.post('http://localhost:8080/api/user/register', objectUser).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllUser();
                    // location.reload()
                    $rootScope.toastSuccess("Created successfully");
                    console.log("insert successfull")
                }

            }, function (response) {
                $scope.toastError("Create unsucessfully");;
                console.log("error");
            });
        }
        //update
        else {
     
            console.log(JSON.stringify(objectUser));
            $http.put('http://localhost:8080/api/user/update/' +  $rootScope.idU, objectUser).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllUser();
                    // location.reload();
                    $rootScope.toastSuccess("Updated successfully");
                    console.log("update successfull");
                }
            }, function (response) {
                $rootScope.toastError("Update unsuccessfully");
                console.log("update unsuccessfull");
            });
        }
    }

    //find by ID
    $scope.getUserByID = function (id) {
       
        $rootScope.idU = id;
        $http.get('http://localhost:8080/api/user/' + $rootScope.idU).then(function (response) {
            if (response.status == 200) {
                $scope.username = response.data.username;
                $scope.password = response.data.password;
                $scope.description = response.data.description;
                $scope.role = response.data.role;
                $scope.fullname = response.data.fullname;
                $scope.address = response.data.address;
                $scope.email = response.data.email;
                $scope.phone = response.data.phoneNumber;
                $scope.btnText = "Update";
                $rootScope.User = response.data;
                // console.log(JSON.stringify($rootScope.User));
            }
        }, function (reponse) {
            console.log("get error");
        });
    }

$scope.deleteUser = function(){
    
    $http.delete('http://localhost:8080/api/user/delete/' + $rootScope.idU).then(function (response) {
        $('#dataTable').DataTable().destroy();
        if (response.status == 200) {
            console.log("sucessful")
           $scope.getAllUser();
           $rootScope.toastSuccess("Deleted sucessfully");
          
        }
    }, function (reponse) {
        console.log("delete error");
        $rootScope.toastError("Delete unsuccessfully");
    });

}


});