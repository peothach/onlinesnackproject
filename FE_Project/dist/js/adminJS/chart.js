 app.controller("ctrlReportChart", function ($scope, $http) {
            $scope.years = [
                { year: "2021", description: "Năm 2021" },
                { year: "2022", description: "Năm 2022" },
                { year: "2023", description: "Năm 2023" },
                { year: "2024", description: "Năm 2024" },
                { year: "2025", description: "Năm 2025" }
            ];

            $scope.changedYear = function (item) {
                $http({
                    method: 'GET',
                    url: "http://localhost:8080/api/category/totalRevenueByYear?year=" + item.year
                }).then(function successCallback(response) {
                    if (response.data.length == 0) {
                        setTimeout(() => {
                            alert("Thống kê đang được cập nhật, vui lòng quay trở lại sau");
                        }, 500);
                    } else {
                        callChart(item.year, response.data);
                    }
                }, function errorCallback(response) {
                    alert("Đã có lỗi xảy ra, vui lòng liên hệ người quản trị")
                });
            };
        });
        function callChart(year, data) {
            var list = data;
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                zoomEnabled: true,
                theme: "dark2",
                title: {
                    text: "Tổng doanh thu của năm " + year
                },
                axisX: {
                    title: "Tháng",
                    valueFormatString: "####",
                    interval: 1
                },
                axisY: {
                    logarithmic: false, //change it to false
                    title: "",
                    prefix: "",
                    titleFontColor: "#6D78AD",
                    lineColor: "#6D78AD",
                    gridThickness: 0,
                    lineThickness: 1,
                    labelFormatter: addSymbols
                },
                legend: {
                    verticalAlign: "top",
                    fontSize: 16,
                    dockInsidePlotArea: true
                },
                data: [{
                    type: "line",
                    xValueFormatString: "####",
                    yValueFormatString: "#,##0.## VND",
                    showInLegend: true,
                    name: "",
                    dataPoints: [
                        { x: 1, y: list[0][0] },
                        { x: 2, y: list[0][1] },
                        { x: 3, y: list[0][2] },
                        { x: 4, y: list[0][3] },
                        { x: 5, y: list[0][4] },
                        { x: 6, y: list[0][5] },
                        { x: 7, y: list[0][6] },
                        { x: 8, y: list[0][7] },
                        { x: 9, y: list[0][8] },
                        { x: 10, y: list[0][9] },
                        { x: 11, y: list[0][10] },
                        { x: 12, y: list[0][11] }
                    ]
                }]
            });
            chart.render();

            function addSymbols(e) {
                var suffixes = ["", "K", "M", "B"];

                var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
                if (order > suffixes.length - 1)
                    order = suffixes.length - 1;

                var suffix = suffixes[order];
                return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
            }
        }