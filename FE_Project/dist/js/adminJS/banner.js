app.controller("ctrlBanner", function ($scope, $rootScope, $http) {
  $scope.listBanner = [];
  $scope.btnText = "";
  $scope.multipartFile = "";

  //get all List Supplier
  $scope.getAllBanner = function () {
    $http({
      method: "GET",
      url: "http://localhost:8080/api/banner/findAll",
    }).then(
      function successCallback(response) {
        $scope.listBanner = response.data;
        $(document).ready(function () {
          $("#dataTable").DataTable();
        });
      },
      function errorCallback(response) {
        console.log("Error");
      }
    );
  };
  //clear object Supplier
  $scope.Clear = function () {
    $scope.titleBanner = "";
    $scope.descriptionBanner = "";
    $scope.multipartFile = "";
  };
  //get all List User
  $scope.getAllBanner();

  $scope.getButton = function () {
    $scope.btnText = "Save";
    $scope.Clear();
    // alert($scope.btnText);
  };

  //insert banner
  $scope.saveUpdate = function () {
    var config = {
      headers: { "Content-Type": undefined },
    };

    var objectBanner = {
      title: $scope.titleBanner,
      description: $scope.descriptionBanner,
      multipartFile: $scope.image,
    };

    //save
    if ($scope.btnText == "Save") {


      var data = new FormData();

      data.append("title", objectBanner.title);
      data.append("description", objectBanner.description);
      data.append("multipartFile", $scope.image);
  

      $http({
        method: "POST",
        url: "http://localhost:8080/api/banner/create",
        data: data,
        headers: { "Content-Type": undefined },
      }).then(
        function successCallback(response) {
          $("#dataTable").DataTable().destroy();
          console.log("Uploaded");
          $scope.getAllBanner();
          // location.reload();
          $rootScope.toastSuccess("Created successfully");
          console.log("insert successfull");
        },
        function errorCallback(response) {
            $rootScope.toastError("Create unsuccessfully");
          console.log("uppload unsuccessful");
          console.log(response.message);
        }
      );
    }
    //update
    else {
      $scope.idBanner = localStorage.getItem("idBanner");
      $http
        .put(
          "http://localhost:8080/api/banner/update/" + $scope.idBanner,
          data,
          config
        )
        .then(
          function (response) {
            if (response.status == 200) {
              $("#dataTable").DataTable().destroy();
              $scope.getAllBanner();
              // location.reload();
              $rootScope.toastSuccess("Updated successfully");
              console.log("update successfull");
            }
          },
          function (reponse) {
            console.log("update unsuccessfull");
            $rootScope.toastError("Update unsuccessfully");
          }
        );
    }
  };

  //find by ID
  $scope.getBannerByID = function (id) {
    $http.get("http://localhost:8080/api/banner/" + id).then(
      function (response) {
        if (response.status == 200) {
          $rootScope.idBanner = id;
          $scope.id = response.data.id;
          $scope.titleBanner = response.data.title;
          $scope.descriptionBanner = response.data.description;
          $scope.image = response.data.image;
          $scope.btnText = "Update";
        }
      },
      function (reponse) {
        console.log("get error");
      }
    );
  };
  //getID to delete
  $scope.getIDBanner = function (id) {
    // localStorage.setItem("idBannerToDelete", id);
    $rootScope.idBn = id;
  };
  //update image
  $scope.uploadFile = function (event) {
    var files = event.target.files;
    $scope.image = files[0];
    console.log($scope.image);
  };

  //delete supplier by id
  $scope.deleteBanner = function () {
    $http
      .delete("http://localhost:8080/api/banner/delete/" + $rootScope.idBn)
      .then(
        function (response) {
          if (response.status == 200) {
            $("#dataTable").DataTable().destroy();
            console.log("sucessful");
            $scope.getAllBanner();
            //    location.reload();
            $rootScope.toastSuccess("Deleted successfully");
          }
        },
        function (response) {
          console.log("delete error");
          console.log(response.message);
          $rootScope.toastError("Delete unsuccessfully");
        }
      );
  };
});
app.directive("selectNgFiles", function () {
  return {
    require: "ngModel",
    link: function postLink(scope, elem, attrs, ngModel) {
      elem.on("change", function (e) {
        var files = elem[0].files;
        ngModel.$setViewValue(files);
      });
    },
  };
});
app.directive("customOnChange", function () {
  return {
    restrict: "A",
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind("change", onChangeHandler);
    },
  };
});
