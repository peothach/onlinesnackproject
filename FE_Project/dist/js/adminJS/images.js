app.controller("ctrlImages", function ($scope,$rootScope, $http) {
    $scope.url_string = location.href;
    console.log($scope.url_string);
    $scope.idProduct = $scope.url_string.split("id=")[1];
    $scope.nameProduct;
    $scope.allImages = [];

  
     //get product by id
  $scope.getNameProductByID = function () {

    $http.get("http://localhost:8080/api/product/" + $scope.idProduct).then(
      function (response) {
        if (response.status == 200) {
          $scope.nameProduct = response.data.name;    
        }
      },
      function (reponse) {
        console.log("get error");
      }
    );
  };

  $scope.getNameProductByID();
    // $scope.nameProduct = localStorage.getItem("nameProduct");
    // $scope.idProduct = localStorage.getItem("viewID");
    // $scope.multipartFile = [];
    var multipartFile = new Array();
   
    //onchange upload image
    $(document).on('change','.up', function(){
        var names = [];
        var file = [];
       
        var length = $(this).get(0).files.length;
       
          for (var i = 0; i < $(this).get(0).files.length; ++i) {
              names.push($(this).get(0).files[i].name);
               file = $(this).get(0).files[i];
               multipartFile.push(file);
               console.log(multipartFile);
              
              
            //  console.log($scope.multipartFile);
          }
         
  
          // $("input[name=file]").val(names);
        if(length>2){
          var fileName = names.join(', ');
          $(this).closest('.form-group').find('.form-control').attr("value",length+" files selected");
        }
        else{
          $(this).closest('.form-group').find('.form-control').attr("value",names);
        }
     });
    $scope.getAllImages = function () {
        
        $http({
            method: 'GET',
            url: 'http://localhost:8080/api/imageProduct/viewImageProduct?id='+ $scope.idProduct,
        }).then(function successCallback(response) {
            $scope.allImages = response.data;
            // $rootScope.toastSuccess();
            console.log(response.data);
            // console.log("success");
            
            
            //alert(JSON.stringify($scope.product));
        }, function errorCallback(response) {
            $rootScope.toastError();
        });
    } 
    //show all img product on page
    $scope.getAllImages();
    //update status img
    $scope.updateStatusImg = function(){
        //get img by id
        var dataUpdate = new FormData();
        // let newFile = new File([], "");
        dataUpdate.append("idImage",$rootScope.idImages);
        dataUpdate.append("productIdImage",$scope.idProduct);
        dataUpdate.append("statusImage", 1);
        // dataUpdate.append('multipartFile', newFile);

         $http({
            method: 'PUT',
            url: 'http://localhost:8080/api/imageProduct/update',
            data: dataUpdate,
            headers: {'Content-Type': undefined},
          }).then(function successCallback(response) {
            console.log('Updated');
                          $scope.getAllImages();
                          $rootScope.toastSuccess("Updated successfully");
        // location.reload();
        // $scope.toastSuccess();
   
            }, function errorCallback(response) {
                $rootScope.toastError("Update unsuccessfully");
             console.log("update unsuccessful");
             console.log(response.message);
            });
    }
    //create img product
    $scope.create = function(){

        var data = new FormData(); 
        data.append("statusImage", 0);
        data.append("productIdImage", $scope.idProduct);
        for(var i = 0;i<multipartFile.length;i++){
            data.append("multipartFile", multipartFile[i]);
        }
      
        console.log(JSON.stringify(data));
        //call API
        $http({
            method: 'POST',
            url: 'http://localhost:8080/api/imageProduct/create',
            data: data,
            headers: {'Content-Type': undefined},
          }).then(function successCallback(response) {
            console.log('Uploaded');
            $(this).closest('.form-group').find('.form-control').attr("value","");
            $rootScope.toastSuccess("Created successfully");
                          $scope.getAllImages();
                         
        // location.reload();
        // $scope.toastSuccess();
        console.log("insert successfull")
            }, function errorCallback(response) {
                $rootScope.toastError("Create unsuccessfully");
             console.log("uppload unsuccessful");
             console.log(response.message);
            });
        
    }
    //get id img to delete
    $scope.getIDImages = function(id){
      $rootScope.idImages = id;
    }
    //delete img
    $scope.deleteImages = function(id){
        $http.delete('http://localhost:8080/api/imageProduct/delete/' + $rootScope.idImages).then(function (response) {
            if (response.status == 200) {
                console.log("sucessful")
               $scope.getAllImages();
               $rootScope.toastSuccess("Deleted sucessfully");
            //    location.reload();
          
            }
        }, function (reponse) {
            console.log("delete error");
            $rootScope.toastError("Delete unsuccessfully");
        });
    
    }

});