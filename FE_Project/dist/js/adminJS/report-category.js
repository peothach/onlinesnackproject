app.controller("ctrlReportCategory", function ($scope, $http) {
    var reportType = "";
    var year = "";
    $scope.listCategories = [];
    $scope.reportTypes = [
        { type: "CATEGORY", description: "Loại" },
        { type: "SUPPLIER", description: "Nhà cung cấp" }
    ];

    $scope.years = [
        { year: "2020", description: "Năm 2021" },
        { year: "2021", description: "Năm 2021" },
        { year: "2022", description: "Năm 2022" },
        { year: "2023", description: "Năm 2023" },
        { year: "2024", description: "Năm 2024" },
        { year: "2025", description: "Năm 2025" }
    ];

    $scope.changedType = function (item) {
        reportType = item.type;
    };

    $scope.changedYear = function (item) {
        year = item.year;
    };

    $scope.filter = function () {
        if (reportType !== "" && year !== "") {
            switch (reportType) {
                case 'CATEGORY':
                    callAPI("http://localhost:8080/api/category/report?year=" + year);
                    break;
                case 'SUPPLIER':
                    callAPI("http://localhost:8080/api/supplier/report?year=" + year);
                    break;
                default:
                    break;

            }
        } else if (reportType === "") {
            alert("Vui lòng chọn kiểu thống kê");
        } else if (year === "") {
            alert("Vui lòng chọn năm");
        }
    }

    function callAPI(url) {
        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            if(response.data.length == 0){
                setTimeout(() => {
                    alert("Không có dữ liệu");
                }, 500);
            }else{
                $scope.listCategories = response.data;
                $(document).ready(function() {
                    $('#dataTable').DataTable();
                  });
                
            }
        }, function errorCallback(response) {
            alert("Đã có lỗi xảy ra, vui lòng liên hệ người quản trị")
        });
    }
});