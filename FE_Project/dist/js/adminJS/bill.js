
app.controller("ctrlBill", function ($scope, $rootScope, $http, $rootScope) {
    $scope.bill = [];
    $scope.viewBill = [];
    $rootScope.idBill;
    //get all bill
    $scope.getAllBill = function () {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/api/cart/all'
        }).then(function successCallback(response) {
            $scope.bill = response.data;

            //alert(JSON.stringify($scope.product));
        }, function errorCallback(response) {

        });
    }
    $scope.stripDate = function(date) {
        return date.substring(0,10);
      }
    $scope.getAllBill();

    //get bill by id

    $scope.getBillByID = function (id) {

        
        $http.get('http://localhost:8080/api/cart/viewBill/' + id).then(function (response) {
            if (response.status == 200) {
                $scope.viewBill = response.data; 
                $rootScope.idBill = id;
                // $scope.id = response.data.id;
                // $scope.date = response.data.creatDate;
                // $scope.fullname = response.data.billId;
                // $scope.product = response.data.productId;
                
                
            }
        }, function (response) {
            console.log("get error");
        });
    }
    
    //update status bill
    $scope.updateStatusBill = function(status){
        // var today = new Date();
        // var date = today.getDate();
        // var month = today.getMonth();
        // var year = today.getFullYear();
        // var current_date = date + "/" + month + "/" + year;
        var objectBill = {
            // create_at: "",
            // description: "",
            // user_id: "1",
            // billId: id,
            status: status
        }
        
         $http.put('http://localhost:8080/api/bill/update/' + $rootScope.idBill, objectBill).then(function (response) {
                if (response.status == 200) {
                    $scope.getAllBill();;
                    // location.reload();
                    $rootScope.toastSuccess("Updated successfully");
                    console.log("update successfull");
                }
            }, function (reponse) {
                console.log("update unsuccessfull");
                $rootScope.toastError("Update unsuccessfully");
            });
    }

 

});