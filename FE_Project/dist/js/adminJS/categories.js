app.controller("ctrlSupplier", function ($scope, $rootScope, $http) {
    $scope.listCategories = [];
  
    $scope.btnText = "";

    //get all List Supplier
    $scope.getAllCategory = function () {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/api/category/all'
        }).then(function successCallback(response) {
            $scope.listCategories = response.data;
            $(document).ready(function() {
                $('#dataTable').DataTable();
              });

        }, function errorCallback(response) {
            console.log("Error");
            $scope.toastError();
        });
    }
    //clear object Supplier
    $scope.Clear = function () {
        $scope.nameCategory = "";
        $scope.iconCategory = "";
        
      
    }
    //get all List User
    $scope.getAllCategory();


    $scope.getButton = function () {
        $scope.btnText = "Save";
        $scope.Clear();
        // alert($scope.btnText);
    }

    //insert user
    $scope.saveUpdate = function () {
        var objectCategory = {
            name: $scope.nameCategory,
            icon: $scope.iconCategory,
           
        }
        //save
        if ($scope.btnText == "Save") {
            $http.post('http://localhost:8080/api/category/create', objectCategory).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllCategory();
                    // location.reload();
                    $rootScope.toastSuccess("Created successfully");
                    console.log("insert successfull")
                }

            }, function (response) {
                console.log("error");
                $rootScope.toastError("Create unsuccessfully");
            });
        }
        //update
        else {
           
            $http.put('http://localhost:8080/api/category/update/' +  $rootScope.idCategory, objectCategory).then(function (response) {
                if (response.status == 200) {
                    $('#dataTable').DataTable().destroy();
                    $scope.getAllCategory();
                    // location.reload();
                    console.log("update successfull");
                    $rootScope.toastSuccess("Updated successfully");
                }
            }, function (reponse) {
                console.log("update unsuccessfull");
                $rootScope.toastError("Update unsuccessfully");
            });
        }
    }

    //find by ID
    $scope.getCategoryByID = function (id) {
    
        $http.get('http://localhost:8080/api/category/' + id).then(function (response) {
            if (response.status == 200) {
                $rootScope.idCategory = id;
                console.log($rootScope.idCategory);
                $scope.nameCategory = response.data.name;     
                $scope.iconCategory = response.data.icon; 
                $scope.btnText = "Update";
              
            }
        }, function (reponse) {
            console.log("get error");
        });
    }
  

//delete supplier by id
$scope.deleteCategory = function(){
        $http.delete('http://localhost:8080/api/category/deletecategory/' + $rootScope.idCategory).then(function (response) {
            if (response.status == 200) {
                $('#dataTable').DataTable().destroy();
                console.log("sucessful")
               $scope.getAllCategory();
               $rootScope.toastSuccess("Deleted successfully");
              
            }
        }, function (reponse) {
            console.log("delete error");
            $rootScope.toastError("Delete unsuccessfully");
        });
    
    }
});
